# -*- encoding: utf-8 -*-
import json

from braces.views import (
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    SuperuserRequiredMixin,
)
from django.core.serializers.json import DjangoJSONEncoder
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.generic import DetailView, ListView, UpdateView
from xero.exceptions import XeroException

from base.view_utils import BaseMixin
from invoice.forms import BatchEmptyForm, InvoiceEmptyForm
from invoice.models import Batch, Invoice
from invoice.views import InvoiceDetailMixin
from .forms import (
    BatchAccountUpdateForm,
    XeroCurrencyUpdateForm,
    XeroSettingsForm,
    XeroVatCodeUpdateForm,
)
from .models import (
    BatchAccount,
    BatchPaymentUUID,
    BatchUUID,
    get_xero_credit_note_data,
    get_xero_invoice_data,
    get_xero_invoice_url,
    get_xero_payment_data,
    InvoiceUUID,
    PaymentUUID,
    XeroCurrency,
    XeroSettings,
    XeroVatCode,
)
from .tasks import (
    xero_sync_batch,
    xero_sync_batch_payment,
    xero_sync_invoice,
    xero_sync_payment,
)


class BatchAccountListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):

    model = BatchAccount
    paginate_by = 20


class BatchAccountUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):

    form_class = BatchAccountUpdateForm
    model = BatchAccount

    def get_success_url(self):
        return reverse("xero.account.list")


class XeroBatchDetailMixin:
    def _batch(self):
        raise NotImplementedError(
            "Need to create a '_batch' method to return the 'Batch'"
        )

    def _xero_batch_uuid(self):
        uuid = url = None
        try:
            batch_uuid = BatchUUID.objects.get(batch=self._batch())
            uuid = str(batch_uuid.uuid)
            if uuid:
                url = get_xero_invoice_url(uuid)
        except BatchUUID.DoesNotExist:
            pass
        return uuid, url

    def _xero_payment_uuid(self):
        result = None
        try:
            payment_uuid = BatchPaymentUUID.objects.get(batch=self._batch())
            result = str(payment_uuid.uuid)
        except BatchPaymentUUID.DoesNotExist:
            pass
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        xero_batch_uuid, xero_batch_url = self._xero_batch_uuid()
        context.update(
            dict(
                xero_batch_url=xero_batch_url,
                xero_batch_uuid=xero_batch_uuid,
                xero_payment_uuid=self._xero_payment_uuid(),
            )
        )
        return context


class XeroBatchDetailView(
    LoginRequiredMixin,
    SuperuserRequiredMixin,
    XeroBatchDetailMixin,
    BaseMixin,
    DetailView,
):
    model = Batch
    template_name = "xero_app/batch_detail.html"

    def _batch(self):
        return self.object


class XeroBatchSyncPaymentUpdateView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, UpdateView
):
    model = Batch
    form_class = BatchEmptyForm
    template_name = "xero_app/batch_form_sync_to_xero.html"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        xero_sync_batch_payment.send(self.object.pk)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("xero.batch.detail", args=[self.object.pk])


class XeroBatchSyncUpdateView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, UpdateView
):
    model = Batch
    form_class = BatchEmptyForm
    template_name = "xero_app/batch_form_sync_to_xero.html"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        xero_sync_batch.send(self.object.pk)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("xero.batch.detail", args=[self.object.pk])


class XeroCurrencyListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):

    model = XeroCurrency
    paginate_by = 20


class XeroCurrencyUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):

    form_class = XeroCurrencyUpdateForm
    model = XeroCurrency

    def get_success_url(self):
        return reverse("xero.currency.list")


class XeroInvoiceDetailMixin:
    def _xero_invoice_uuid(self):
        uuid = url = None
        try:
            invoice_uuid = InvoiceUUID.objects.get(invoice=self.object)
            uuid = str(invoice_uuid.uuid)
            if uuid:
                url = get_xero_invoice_url(uuid)
        except (InvoiceUUID.DoesNotExist, XeroException):
            pass
        return uuid, url

    def _xero_payment_uuid(self):
        result = None
        try:
            payment_uuid = PaymentUUID.objects.get(invoice=self.object)
            result = str(payment_uuid.uuid)
        except PaymentUUID.DoesNotExist:
            pass
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        xero_invoice_uuid, xero_invoice_url = self._xero_invoice_uuid()
        xero_payment_uuid = self._xero_payment_uuid()
        context.update(
            dict(
                xero_invoice_url=xero_invoice_url,
                xero_invoice_uuid=xero_invoice_uuid,
                xero_payment_uuid=xero_payment_uuid,
            )
        )
        return context


class XeroInvoiceDetailView(
    LoginRequiredMixin,
    SuperuserRequiredMixin,
    InvoiceDetailMixin,
    XeroInvoiceDetailMixin,
    BaseMixin,
    DetailView,
):
    model = Invoice
    template_name = "xero_app/invoice_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # invoice
        xero_invoice_data = None
        xero_invoice_uuid, url = self._xero_invoice_uuid()
        if xero_invoice_uuid:
            if self.object.is_credit:
                data = get_xero_credit_note_data(xero_invoice_uuid)
            else:
                data = get_xero_invoice_data(xero_invoice_uuid)
            xero_invoice_data = json.dumps(
                data, indent=4, cls=DjangoJSONEncoder
            )
        # payment
        xero_payment_data = None
        xero_payment_uuid = self._xero_payment_uuid()
        if xero_payment_uuid:
            data = get_xero_payment_data(xero_payment_uuid)
            xero_payment_data = json.dumps(
                data, indent=4, cls=DjangoJSONEncoder
            )
        context.update(
            dict(
                xero_invoice_data=xero_invoice_data,
                xero_payment_data=xero_payment_data,
            )
        )
        return context


class XeroInvoiceSyncPaymentUpdateView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, UpdateView
):
    model = Invoice
    form_class = InvoiceEmptyForm
    template_name = "xero_app/invoice_form_sync_to_xero.html"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        xero_sync_payment.send(self.object.pk)
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        caption = "Refund" if self.object.is_credit else "Payment"
        context.update(dict(caption=caption))
        return context

    def get_success_url(self):
        return reverse("xero.invoice.detail", args=[self.object.pk])


class XeroInvoiceSyncUpdateView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, UpdateView
):
    model = Invoice
    form_class = InvoiceEmptyForm
    template_name = "xero_app/invoice_form_sync_to_xero.html"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        xero_sync_invoice.send(self.object.pk)
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        caption = "Credit Note" if self.object.is_credit else "Invoice"
        context.update(dict(caption=caption))
        return context

    def get_success_url(self):
        return reverse("xero.invoice.detail", args=[self.object.pk])


class XeroSettingsUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):

    form_class = XeroSettingsForm

    def get_object(self):
        return XeroSettings.load()

    def get_success_url(self):
        return reverse("project.settings")


class XeroVatCodeListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    """List of Xero VAT codes."""

    model = XeroVatCode
    paginate_by = 20


class XeroVatCodeUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):

    form_class = XeroVatCodeUpdateForm
    model = XeroVatCode

    def get_success_url(self):
        return reverse("xero.vat.code.list")
