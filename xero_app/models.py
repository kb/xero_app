# -*- encoding: utf-8 -*-
import json
import logging
import os
import time
import urllib.parse

from datetime import date
from dateutil.relativedelta import relativedelta
from decimal import Decimal, ROUND_UP
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.core.serializers.json import DjangoJSONEncoder
from django.db import models, transaction
from django.utils import timezone
from django.utils.text import slugify
from reversion import revisions as reversion
from xero import Xero
from xero.auth import PrivateCredentials
from xero.exceptions import XeroException

from base.model_utils import TimeStampedModel
from base.singleton import SingletonModel
from contact.models import Contact
from finance.models import Currency, VatCode
from invoice.models import (
    Batch,
    BatchInvoice,
    Invoice,
    InvoiceContact,
    InvoiceCredit,
    InvoiceIssue,
    PaymentProcessor,
)

logger = logging.getLogger(__name__)


class XeroError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr("%s, %s" % (self.__class__.__name__, self.value))


def _xero_time(d):
    return d.isoformat()


def connect():
    """Connect to the Xero API.

    The API we are using (https://github.com/freakboy3742/pyxero/) does not have
    access to the rate limit information in the API responses.
    For now, we will ``sleep`` for 1 second each time we connect.
    This will stop us exceeding 60 calls per minutes.

    OAuth2.0 API Limits
    https://developer.xero.com/documentation/oauth2/limits

    API Rate Limits

      There are limits to the number of API calls that your application can make
      against a particular tenant (organisation or practice):

    - Concurrent Limit: 5 calls in progress at one time
    - Minute Limit: 60 calls per minute
    - Daily Limit: 5000 calls per day

    Each API response you receive will include the ``X-DayLimit-Remaining`` and
    ``X-MinLimit-Remaining`` headers telling you the number of remaining against
    each limit.

    """
    file_name = settings.XERO_KEY_FILE
    if os.path.exists(file_name) and os.path.isfile(file_name):
        with open(file_name) as keyfile:
            rsa_key = keyfile.read()
        credentials = PrivateCredentials(settings.XERO_CONSUMER_KEY, rsa_key)
        time.sleep(1)
        xero = Xero(credentials)
        return xero
    else:
        raise XeroError("Xero key file ('{}') does not exist".format(file_name))


def dates_to_sync():
    result = []
    xero_settings = XeroSettings.load()
    if not xero_settings.days_to_sync:
        raise XeroError("Xero settings have not been updated")
    today = date.today()
    for x in range(0, xero_settings.days_to_sync):
        result.append(today + relativedelta(days=-(x + 1)))
    result.reverse()
    return result


def get_xero_contacts():
    xero = connect()
    return xero.contacts.all()


def get_xero_credit_note_data(uuid):
    xero = connect()
    return xero.creditnotes.get(str(uuid))


def get_xero_exchange_rate(exchange_rate):
    result = Decimal("1") / exchange_rate
    return str(result.quantize(Decimal(".0001"), rounding=ROUND_UP))


def get_xero_invoice_data(uuid):
    xero = connect()
    return xero.invoices.get(str(uuid))


def get_xero_invoice_url(uuid):
    """Get the URL of a Xero invoice.

    Using the workaround in:
    https://github.com/freakboy3742/pyxero/issues/134

    """
    result = None
    data = get_xero_invoice_data(str(uuid) + "/OnlineInvoice")
    if "OnlineInvoices" in data:
        value = data["OnlineInvoices"]
        if len(value):
            row = value[0]
            if "OnlineInvoiceUrl" in row:
                result = row["OnlineInvoiceUrl"]
    return result


def get_xero_payment_data(uuid):
    xero = connect()
    return xero.payments.get(str(uuid))


def sync_xero(invoice_date):
    """Sync Xero invoice batches and invoices.

    .. note:: We might want to move ``init_batch_account`` into a separate
              earlier task as it will create new records.
              The operator might need to update the accounts before the sync
              can successfully complete.

    """
    count = 0
    logger.info("sync_xero: {}".format(invoice_date.strftime("%d/%m/%Y")))
    BatchAccount.objects.init_batch_account(invoice_date)
    # batch
    sync_batch = SyncBatch()
    sync_batch.sync_date(invoice_date)
    # invoice
    sync_invoice = SyncInvoice()
    sync_invoice.sync_date(invoice_date)
    # credit note
    sync_credit = SyncCredit()
    sync_credit.sync_date(invoice_date)
    return count


class SyncXero:
    def _api_result(self, data, uuid_field_name, check_uuid=None):
        """Check the response from Xero and try to get the UUID.

        If ``check_uuid`` is passed we are updating a record.  As a double check
        we make sure the UUID returned from Xero is the same as the one we are
        expecting to update.

        .. note:: Previously called ``xero_api_result``.

        """
        uuid = None
        if len(data) == 1:
            row = data[0]
            if uuid_field_name in row:
                uuid = row[uuid_field_name]
            if not uuid:
                raise XeroError(
                    "Cannot find uuid in result using '{}': {}".format(
                        uuid_field_name, row
                    )
                )
        else:
            raise XeroError(
                "No data (or too much data) returned from Xero: {}".format(data)
            )
        if check_uuid:
            # check we updated the correct record!
            if not check_uuid == uuid:
                raise XeroError(
                    "Original uuid '{}' does not match Xero uuid: '{}'".format(
                        check_uuid, uuid
                    )
                )
        return uuid

    def _contact_to_xero_data(self, contact, uuid=None):
        """Convert a contact model to a Xero compatible dictionary.

        - We use the ``uuid`` when we want to update (``save``) a contact.
        - Leave the ``uuid`` set to ``None`` when creating a contact as Xero will
          auto generate it.

        .. note:: Previously called ``contact_to_xero_data``.

        """
        email = contact.current_email()
        result = {}
        if uuid:
            result.update({"ContactID": uuid})
        result.update(
            {
                "ContactNumber": contact.pk,
                "ContactStatus": "ACTIVE",
                "Name": contact.full_name,
                "FirstName": contact.user.first_name,
                "LastName": contact.user.last_name,
                "EmailAddress": email.email if email else "",
                "BankAccountDetails": "",
                "UpdatedDateUTC": timezone.now().isoformat(),
                "ContactGroups": [],
                "IsSupplier": False,
                "IsCustomer": True,
                "ContactPersons": [],
                "HasAttachments": False,
                "HasValidationErrors": False,
                "Url": urllib.parse.urljoin(
                    settings.HOST_NAME, contact.get_absolute_url()
                ),
            }
        )
        # addresses
        addresses = []
        for address in contact.current_addresses().order_by("-pk"):
            tags = [x.name.lower() for x in address.tags.all()]
            addresses.append(
                {
                    "AddressType": "POBOX" if "invoice" in tags else "STREET",
                    "AddressLine1": address.address_one,
                    "AddressLine2": address.address_two,
                    "AddressLine3": address.locality,
                    "City": address.town,
                    "Region": address.admin_area,
                    "PostalCode": address.postal_code,
                    "Country": address.country,
                    "AttentionTo": "",
                }
            )
        result.update({"Addresses": addresses})
        # phone numbers
        phones = []
        for phone in contact.current_phones().order_by("-pk"):
            tags = [x.name.lower() for x in phone.tags.all()]
            phones.append(
                {
                    "PhoneType": "MOBILE" if "mobile" in tags else "DEFAULT",
                    "PhoneNumber": phone.phone,
                    "PhoneAreaCode": "",
                    "PhoneCountryCode": "",
                }
            )
        result.update({"Phones": phones})
        return result

    def _sync_contact_to_xero(self, contact):
        """

        .. note:: Previously called ``sync_contact_to_xero``.

        """
        xero = connect()
        # does this contact already exist on Xero?
        contact_uuid = ContactUUID.objects.get_if_exists(contact)
        if contact_uuid:
            logger.info(
                "sync_xero: update contact '{}' ({})".format(
                    contact.user.username, contact.pk
                )
            )
            uuid = contact_uuid.uuid_as_str()
            data = self._contact_to_xero_data(contact, uuid)
            # update an existing contact record on Xero
            self._api_result(xero.contacts.save(data), "ContactID", uuid)
        else:
            logger.info(
                "sync_xero: create contact '{}' ({})".format(
                    contact.user.username, contact.pk
                )
            )
            # create a new contact record on Xero
            data = self._contact_to_xero_data(contact)
            with transaction.atomic():
                ContactUUID.objects.init_contact(contact)
                uuid = self._api_result(xero.contacts.put(data), "ContactID")
            # assign the uuid from Xero to the contact
            ContactUUID.objects.update_uuid(contact, uuid)

    def _xero_account_for_invoice(self):
        """

        .. note:: Previously called ``xero_account_for_invoice``.

        """
        invoice_account = None
        xero_settings = XeroSettings.load()
        try:
            invoice_account = xero_settings.invoice_account
        except ObjectDoesNotExist:
            pass
        if not invoice_account:
            raise XeroError("Cannot find a Xero account for invoices")
        return invoice_account

    def _xero_currency_for_invoice(self, invoice):
        """

        .. note:: Previously called ``xero_currency_for_invoice``.

        """
        try:
            return XeroCurrency.objects.get(currency=invoice.currency)
        except XeroCurrency.DoesNotExist:
            raise XeroError(
                "Cannot find the Xero currency for invoice {} "
                "('{}')".format(invoice.pk, invoice.currency.slug)
            )


class SyncBatch(SyncXero):
    """Sync a batch of invoices to Xero.

    ::

      sync_batch = SyncBatch()
      # sync a single batch
      sync_batch.sync(batch)
      # sync all the batches for a date
      sync_batch.sync_date(batch_date)

    """

    def _batch_payment_to_xero_data(self, batch):
        """Convert a batch to a payment in Xero format.

        .. note:: Previously called ``batch_payment_to_xero_data``.

        """
        batch_uuid = BatchUUID.objects.get_if_exists(batch)
        if not batch_uuid:
            raise XeroError(
                "Batch {} has not been created on Xero".format(batch.pk)
            )
        net, vat = batch.net_and_vat()
        xero_account = self._xero_payment_account_for_batch(batch)
        return {
            "Invoice": {"InvoiceID": batch_uuid.uuid_as_str()},
            "Account": xero_account.account_for_xero(),
            "Date": batch.batch_date,
            "Amount": str(net + vat),
        }

    def _batch_to_xero_data(self, batch):
        """Convert a batch to the Xero data format.

        .. note:: Previously called ``batch_to_xero_data``.

        """
        result = {}
        contact = BatchAccount.objects.contact_for_batch(batch)
        contact_uuid = ContactUUID.objects.get_if_exists(contact)
        if not (contact_uuid and contact_uuid.is_synced):
            message = (
                "Contact {} (primary key {}) has not been created on "
                "Xero".format(contact.get_full_name(), contact.pk)
            )
            logger.error(message)
            raise XeroError(message)
        net, vat = batch.net_and_vat()
        gross = net + vat
        xero_account = self._xero_invoice_account_for_batch(batch)
        xero_currency = self._xero_currency_for_batch(batch)
        result.update(
            {
                "Type": "ACCREC",
                "InvoiceNumber": "BATCH{:06d}".format(batch.pk),
                "Reference": "",
                "Payments": [],
                "CreditNotes": [],
                "Prepayments": [],
                "Overpayments": [],
                "AmountDue": gross,
                "AmountPaid": 0.0,
                "AmountCredited": 0.0,
                "CurrencyRate": get_xero_exchange_rate(batch.exchange_rate),
                "HasErrors": False,
                "IsDiscounted": False,
                "HasAttachments": False,
                "Contact": {"ContactID": contact_uuid.uuid_as_str()},
                "Date": batch.batch_date,
                "DueDate": batch.batch_date,
                "Status": "AUTHORISED",
                "LineAmountTypes": "Exclusive",
                "SubTotal": str(net),
                "TotalTax": str(vat),
                "Total": str(gross),
                "UpdatedDateUTC": _xero_time(timezone.now()),
                "CurrencyCode": xero_currency.code,
                "Url": urllib.parse.urljoin(
                    settings.HOST_NAME, batch.get_absolute_url()
                ),
            }
        )
        line_items = []
        vat_analysis = batch.vat_analysis()
        for vat_code, analysis in vat_analysis.items():
            description = "Summary for VAT code '{}' ({} items)".format(
                vat_code, analysis["count"]
            )
            vat = analysis["vat"]
            # I tried removing the ``AccountCode``, but it is *required* here...
            line_item_data = {
                "Description": description,
                "Quantity": str(Decimal("1.00")),
                "UnitAmount": str(analysis["net"]),
                "TaxAmount": str(vat),
                "TaxType": XeroVatCode.objects.vat_code_for(vat_code),
                "AccountCode": xero_account.code,
            }
            line_items.append(line_item_data)
        result.update({"LineItems": line_items})
        return result

    def _sync_batch_payment_to_xero(self, batch):
        """

        .. note:: Previously called ``sync_batch_payment_to_xero``.

        """
        xero = connect()
        # does this batch already exist on Xero?
        batch_uuid = BatchPaymentUUID.objects.get_if_exists(batch)
        if batch_uuid:
            # Don't update a batch payment.
            # It should have posted correctly first time.
            pass
        else:
            logger.info(
                "sync_xero: create batch payment {} for {}".format(
                    batch.pk, batch.batch_date.strftime("%d/%m/%Y")
                )
            )
            # create a new payment record on Xero
            data = self._batch_payment_to_xero_data(batch)
            with transaction.atomic():
                BatchPaymentUUID.objects.init_batch(batch)
                uuid = self._api_result(xero.payments.put(data), "PaymentID")
            # assign the uuid from Xero to the batch
            BatchPaymentUUID.objects.update_uuid(batch, uuid)

    def _xero_currency_for_batch(self, batch):
        """

        .. note:: Previously called ``xero_currency_for_batch``.

        """
        try:
            return XeroCurrency.objects.get(currency=batch.currency)
        except XeroCurrency.DoesNotExist:
            message = (
                "Cannot find the Xero currency for batch {} "
                "('{}')".format(batch.pk, batch.currency.slug)
            )
            logger.error(message)
            raise XeroError(message)

    def _xero_invoice_account_for_batch(self, batch):
        """

        .. note:: Previously called ``xero_invoice_account_for_batch``.

        """
        invoice_account = None
        try:
            batch_account = BatchAccount.objects.get(
                currency=batch.currency,
                payment_processor=batch.payment_processor,
            )
            invoice_account = batch_account.invoice_account
        except BatchAccount.DoesNotExist:
            pass
        if not invoice_account:
            message = (
                "Cannot find a Xero invoice account for batch {} "
                "({})".format(batch.pk, batch.batch_date.strftime("%d/%m/%Y"))
            )
            logger.error(message)
            raise XeroError(message)
        return invoice_account

    def _xero_payment_account_for_batch(self, batch):
        """

        .. note:: Previously called ``xero_payment_account_for_batch``.

        """
        payment_account = None
        try:
            batch_account = BatchAccount.objects.get(
                currency=batch.currency,
                payment_processor=batch.payment_processor,
            )
            payment_account = batch_account.payment_account
        except BatchAccount.DoesNotExist:
            pass
        if not payment_account:
            raise XeroError(
                "Cannot find a Xero payment account for batch {} ({})".format(
                    batch.pk, batch.batch_date.strftime("%d/%m/%Y")
                )
            )
        return payment_account

    def sync(self, batch):
        """Sync a single batch to Xero.

        .. note:: Previously called ``sync_batch_to_xero``

        """
        contact = BatchAccount.objects.contact_for_batch(batch)
        self._sync_contact_to_xero(contact)
        xero = connect()
        # does this batch already exist on Xero?
        batch_uuid = BatchUUID.objects.get_if_exists(batch)
        if batch_uuid:
            # don't update a batch.  It should have posted correctly first time.
            pass
        else:
            logger.info(
                "sync_xero: create batch {} for {}".format(
                    batch.pk, batch.batch_date.strftime("%d/%m/%Y")
                )
            )
            # create a new invoice record on Xero
            data = self._batch_to_xero_data(batch)
            with transaction.atomic():
                BatchUUID.objects.init_batch(batch)
                uuid = self._api_result(xero.invoices.put(data), "InvoiceID")
            # assign the uuid from Xero to the batch
            BatchUUID.objects.update_uuid(batch, uuid)
        # batch payment
        net, vat = batch.net_and_vat()
        if net or vat:
            self._sync_batch_payment_to_xero(batch)

    def sync_date(self, batch_date):
        """Create and sync invoice batches for a day.

        .. note:: Previously called ``sync_xero_batches``.

        """
        count = 0
        batch_count = BatchAccount.objects.create_batches(batch_date)
        if batch_count:
            logger.info(
                "sync_xero: created {} invoice batches".format(batch_count)
            )
        batch_pks = [x.pk for x in Batch.objects.for_date(batch_date)]
        for pk in batch_pks:
            batch = Batch.objects.get(pk=pk)
            logger.info("sync_xero: batch {}".format(batch.pk))
            self.sync(batch)
            count = count + 1
        return count

    def sync_payment(self, batch):
        """Sync a batch payment.

        .. note:: Used only by superuser views to force a sync.

        """
        self._sync_batch_payment_to_xero(batch)


class SyncCredit(SyncXero):
    """Sync credit notes to Xero.

    ::

      sync_credit = SyncCredit()
      # sync a single credit note
      sync_credit.sync(invoice)
      # sync all the credit notes for a date
      sync_credit.sync_date(invoice_date)

    """

    def _batch_account_for_invoice(self, invoice):
        """Find the account details for an invoice in a batch.

        .. note:: Previously called ``batch_account_for_credit_note``.

        """
        batch_invoice = BatchInvoice.objects.get(invoice=invoice)
        batch_account = BatchAccount.objects.get(
            currency=batch_invoice.batch.currency,
            payment_processor=batch_invoice.batch.payment_processor,
        )
        if not batch_account.contact:
            raise XeroError(
                "Batch account '{}' does not have a contact".format(
                    batch_account.pk
                )
            )
        if not batch_account.invoice_account:
            raise XeroError(
                "Batch account '{}' does not have an invoice account".format(
                    batch_account.pk
                )
            )
        if not batch_account.payment_account:
            raise XeroError(
                "Batch account '{}' does not have a payment account".format(
                    batch_account.pk
                )
            )
        return batch_account

    def _check_not_in_a_batch(self, credit_note):
        """If the invoice for the credit note is in a batch, raise an error."""
        try:
            invoice_for_credit = self.invoice_for_credit(credit_note)
            if BatchInvoice.objects.in_a_batch(invoice_for_credit):
                message = (
                    "Credit note '{}' cannot be posted using this method. "
                    "It is linked to invoice '{}' which is in a batch".format(
                        credit_note.pk, invoice_for_credit.pk
                    )
                )
                logger.error(message)
                raise XeroError(message)
        except InvoiceCredit.DoesNotExist:
            pass

    def _check_on_account(self, credit_note):
        """If the credit note cannot be posted on-account, raise an error."""
        on_account = self._is_on_account(credit_note)
        if not on_account:
            message = (
                "Credit note '{}' cannot be posted using this method. "
                "The contact is not setup for 'on_account' postings".format(
                    credit_note.pk
                )
            )
            logger.error(message)
            raise XeroError(message)

    def _credit_note_already_posted(self, credit_note):
        result = False
        credit_note_uuid = InvoiceUUID.objects.get_if_exists(credit_note)
        if credit_note_uuid:
            # don't update a credit note.
            # It should have posted correctly the first time.
            result = True
        return result

    def _credit_note_payment_to_xero_data(
        self, credit_note, uuid, payment_account
    ):
        """Convert a credit note to a Xero refund structure.

        .. note:: Previously called ``credit_note_payment_to_xero_data``.

        """
        result = {
            "CreditNote": {"CreditNoteID": uuid},
            "CurrencyRate": get_xero_exchange_rate(credit_note.exchange_rate),
            "Account": payment_account.account_for_xero(),
            "Date": credit_note.invoice_date,
            "Amount": str(credit_note.gross),
            "Reference": "",
        }
        return result

    def _credit_note_to_xero_data(
        self, credit_note, contact, xero_currency, xero_account_code
    ):
        """

        .. note:: Previously called ``credit_note_to_xero_data``.

        """
        result = {}
        contact_uuid = ContactUUID.objects.get_if_exists(contact)
        if not contact_uuid:
            raise XeroError(
                "Contact {} (primary key {}) has not been created on Xero".format(
                    contact.get_full_name(), contact.pk
                )
            )
        result.update(
            {
                "CreditNoteNumber": "{}".format(credit_note.invoice_number),
                "Payments": [],
                "Type": "ACCRECCREDIT",
                "Reference": "",
                "Contact": {"ContactID": contact_uuid.uuid_as_str()},
                "Date": credit_note.invoice_date,
                "Status": "AUTHORISED",
                "LineAmountTypes": "Exclusive",
                "SubTotal": str(credit_note.net),
                "TotalTax": str(credit_note.vat),
                "Total": str(credit_note.gross),
                "UpdatedDateUTC": _xero_time(timezone.now()),
                "CurrencyCode": xero_currency.code,
                "CurrencyRate": get_xero_exchange_rate(
                    credit_note.exchange_rate
                ),
            }
        )
        line_items = []
        vat_analysis = credit_note.vat_analysis()
        for vat_code, analysis in vat_analysis.items():
            description = "Summary for VAT code '{}' ({} items)".format(
                vat_code, analysis["count"]
            )
            vat = analysis["vat"]
            line_item_data = {
                "AccountCode": xero_account_code,
                "Description": description,
                "LineAmount": str(analysis["net"]),
                "Quantity": str(Decimal("1.0")),
                "TaxAmount": str(vat),
                "TaxType": XeroVatCode.objects.vat_code_for(vat_code),
                "Tracking": [],
                "UnitAmount": str(analysis["net"]),
                "ValidationErrors": [],
            }
            line_items.append(line_item_data)
        result.update({"LineItems": line_items})
        return result

    def _is_on_account(self, credit_note):
        """Is the customer for the credit note setup as on-account."""
        result = False
        try:
            invoice_contact = InvoiceContact.objects.get(
                contact=credit_note.contact
            )
            result = invoice_contact.on_account
        except InvoiceContact.DoesNotExist:
            pass
        return result

    def _post_credit_note(self, credit_note, contact, xero_account_code):
        """Post a credit note to Xero.

        Extracted from ``sync_credit_note_in_batch``

        """
        xero_currency = self._xero_currency_for_invoice(credit_note)
        # create a new credit note record on Xero
        data = self._credit_note_to_xero_data(
            credit_note, contact, xero_currency, xero_account_code
        )
        with transaction.atomic():
            xero = connect()
            InvoiceUUID.objects.init_invoice(credit_note)
            try:
                result = xero.creditnotes.put(data)
                uuid = self._api_result(result, "CreditNoteID")
                logger.info(uuid)
            except XeroException as e:
                logger.exception(e)
                logger.error(json.dumps(data, indent=4, cls=DjangoJSONEncoder))
                raise
        # assign the uuid from Xero to the credit note
        InvoiceUUID.objects.update_uuid(credit_note, uuid)

    def invoice_for_credit(self, credit_note):
        """Get the invoice for the credit note."""
        invoice_credit = InvoiceCredit.objects.get(credit_note=credit_note)
        if not credit_note.contact == invoice_credit.invoice.contact:
            message = (
                "Contact for credit note '{}' ({}) must match the "
                "invoice".format(credit_note.invoice_number, credit_note.pk)
            )
            logger.error(message)
            raise XeroError(message)
        if not credit_note.currency == invoice_credit.invoice.currency:
            message = (
                "Currency for credit note '{}' ({}) must match the "
                "invoice".format(credit_note.invoice_number, credit_note.pk)
            )
            logger.error(message)
            raise XeroError(message)
        return invoice_credit.invoice

    def sync_credit_note(self, credit_note):
        count = 0
        in_a_batch = False
        # is the credit note linked to an invoice?
        try:
            invoice_for_credit = self.invoice_for_credit(credit_note)
            in_a_batch = BatchInvoice.objects.in_a_batch(invoice_for_credit)
        except InvoiceCredit.DoesNotExist:
            pass
        if in_a_batch:
            self.sync_credit_note_in_batch(credit_note)
            self.sync_refund(credit_note)
            count = count + 1
        elif self._is_on_account(credit_note):
            self.sync_credit_note_on_account(credit_note)
            count = count + 1
        else:
            InvoiceIssue.objects.init_invoice_issue(
                credit_note,
                "The credit note is not linked to a batched invoice.",
            )
            InvoiceIssue.objects.init_invoice_issue(
                credit_note,
                "The customer is not setup for on-account postings.",
            )
        return count

    def sync_credit_note_in_batch(self, credit_note):
        """Sync a credit note where the invoice was in a batch.

        .. note:: Previously called ``sync_credit_note_to_xero``.

        """
        if self._credit_note_already_posted(credit_note):
            pass
        else:
            invoice = self.invoice_for_credit(credit_note)
            batch_account = self._batch_account_for_invoice(invoice)
            self._sync_contact_to_xero(batch_account.contact)
            logger.info(
                "sync_xero: create credit note '{}' ({}) "
                "for batch account {}.".format(
                    credit_note.invoice_number, credit_note.pk, batch_account.pk
                )
            )
            self._post_credit_note(
                credit_note,
                batch_account.contact,
                batch_account.invoice_account.code,
            )

    def sync_credit_note_on_account(self, credit_note):
        """Sync a credit note for an on-account customer.

        .. tip:: The definition for on account customer can be found in
                 ``Invoice.objects.on_account_invoices``.

        .. note:: Copied from ``sync_credit_note_in_batch`` for ticket:
                  https://www.kbsoftware.co.uk/crm/ticket/4924/

        """
        count = 0
        if self._credit_note_already_posted(credit_note):
            pass
        else:
            self._check_not_in_a_batch(credit_note)
            self._check_on_account(credit_note)
            self._sync_contact_to_xero(credit_note.contact)
            logger.info(
                "sync_xero: create credit note '{}' ({}) (on-account)".format(
                    credit_note.invoice_number,
                    credit_note.pk,
                )
            )
            xero_account = self._xero_account_for_invoice()
            self._post_credit_note(
                credit_note, credit_note.contact, xero_account.code
            )
            count = 1
        return count

    def sync_date(self, invoice_date):
        """Sync all the credit notes for a date.

        .. note:: Previously called ``sync_xero_credit_notes``.

        """
        count = 0
        credit_pks = [x.pk for x in Invoice.objects.credit_notes(invoice_date)]
        logger.info("sync_xero: {} credit notes".format(len(credit_pks)))
        for pk in credit_pks:
            credit_note = Invoice.objects.get(pk=pk)
            count = count + self.sync_credit_note(credit_note)
        return count

    def sync_refund(self, credit_note):
        """Sync a credit note refund (where the invoice was in a batch).

        .. note:: Is also used by superuser views to force a sync.

        .. note:: Previously called ``sync_credit_note_refund_to_xero``.

        """
        xero = connect()
        if not credit_note.is_credit:
            message = "A refund is not valid for an invoice '{}' ({})".format(
                credit_note.invoice_number, credit_note.pk
            )
            logger.error(message)
            raise XeroError(message)
        invoice = self.invoice_for_credit(credit_note)
        batch_account = self._batch_account_for_invoice(invoice)
        try:
            credit_note_uuid = InvoiceUUID.objects.get(invoice=credit_note)
        except InvoiceUUID.DoesNotExist:
            message = (
                "Credit note '{}' ({}) has not been posted to "
                "Xero".format(credit_note.invoice_number, credit_note.pk)
            )
            logger.error(message)
            raise XeroError(message)
        data = self._credit_note_payment_to_xero_data(
            credit_note, credit_note_uuid.uuid, batch_account.payment_account
        )
        # does this payment already exist on Xero?
        payment_uuid = PaymentUUID.objects.get_if_exists(credit_note)
        if payment_uuid:
            # don't update a payment. It should have posted correctly first time.
            pass
        elif credit_note.gross == Decimal():
            logger.info(
                "sync_xero: credit note value is zero '{}' ({})".format(
                    credit_note.invoice_number, credit_note.pk
                )
            )
        else:
            logger.info(
                "sync_xero: create refund for credit note '{}' ({})".format(
                    credit_note.invoice_number, credit_note.pk
                )
            )
            with transaction.atomic():
                PaymentUUID.objects.init_invoice(credit_note)
                try:
                    result = xero.payments.put(data)
                except XeroException as e:
                    logger.exception(e)
                    logger.error(
                        json.dumps(data, indent=4, cls=DjangoJSONEncoder)
                    )
                    raise
                uuid = self._api_result(result, "PaymentID")
            # assign the uuid from Xero to the invoice
            PaymentUUID.objects.update_uuid(credit_note, uuid)


class SyncInvoice(SyncXero):
    """Sync non-batch invoices to Xero.

    ::

      sync_invoice = SyncInvoice()
      # sync a single invoice
      sync_invoice.sync(invoice)
      # sync all the non-batch invoices for a date
      sync_invoice.sync_date(invoice_date)

    """

    def _invoice_to_xero_data(self, invoice, xero_currency, xero_account_code):
        """

        .. note:: We are not posting individual lines to Xero (for now).
                  This is partly to minimise the number of lines, but also to avoid
                  issues with rounding.

        .. note:: Previously called ``invoice_to_xero_data``.

        """
        result = {}
        contact_uuid = ContactUUID.objects.get_if_exists(invoice.contact)
        if not contact_uuid:
            raise XeroError(
                "Contact {} (primary key {}) has not been created on Xero".format(
                    invoice.contact.get_full_name(), invoice.contact.pk
                )
            )
        result.update(
            {
                "Type": "ACCREC",
                "InvoiceNumber": invoice.invoice_number,
                "Reference": "",
                "Payments": [],
                "CreditNotes": [],
                "Prepayments": [],
                "Overpayments": [],
                "AmountDue": invoice.gross,
                "AmountPaid": 0.0,
                "AmountCredited": 0.0,
                "CurrencyRate": get_xero_exchange_rate(invoice.exchange_rate),
                "HasErrors": False,
                "IsDiscounted": False,
                "HasAttachments": False,
                "Contact": {"ContactID": contact_uuid.uuid_as_str()},
                "Date": invoice.invoice_date,
                "DueDate": invoice.invoice_date,
                "Status": "AUTHORISED",
                "LineAmountTypes": "Exclusive",
                "SubTotal": str(invoice.net),
                "TotalTax": str(invoice.vat),
                "Total": str(invoice.gross),
                "UpdatedDateUTC": _xero_time(timezone.now()),
                "CurrencyCode": xero_currency.code,
                "Url": urllib.parse.urljoin(
                    settings.HOST_NAME, invoice.get_absolute_url()
                ),
            }
        )
        line_items = []
        vat_analysis = invoice.vat_analysis()
        for vat_code, analysis in vat_analysis.items():
            description = "Summary for VAT code '{}' ({} items)".format(
                vat_code, analysis["count"]
            )
            vat = analysis["vat"]
            # I tried removing the ``AccountCode``, but it is *required* here...
            line_item_data = {
                "AccountCode": xero_account_code,
                "Description": description,
                "Quantity": str(Decimal("1.00")),
                "TaxAmount": str(vat),
                "TaxType": XeroVatCode.objects.vat_code_for(vat_code),
                "UnitAmount": str(analysis["net"]),
            }
            line_items.append(line_item_data)
        result.update({"LineItems": line_items})
        return result

    def sync_invoice(self, invoice):
        """

        .. note:: Previously called ``sync_invoice_to_xero``.

        """
        if invoice.is_credit:
            message = (
                "Credit note '{}' ({}) cannot be posted as an "
                "invoice".format(invoice.invoice_number, invoice.pk)
            )
            logger.error(message)
            raise XeroError(message)
        if invoice.upfront_payment:
            message = (
                "Invoice '{}' ({}) was paid upfront, so must be posted as "
                "part of a batch".format(invoice.invoice_number, invoice.pk)
            )
            logger.error(message)
            raise XeroError(message)
        self._sync_contact_to_xero(invoice.contact)
        # does this invoice already exist on Xero?
        invoice_uuid = InvoiceUUID.objects.get_if_exists(invoice)
        if invoice_uuid:
            # don't update an invoice.  It should have posted correctly first time.
            pass
        else:
            logger.info(
                "sync_xero: create invoice '{}' ({}).".format(
                    invoice.invoice_number, invoice.pk
                )
            )
            xero_account = self._xero_account_for_invoice()
            xero_currency = self._xero_currency_for_invoice(invoice)
            # create a new invoice record on Xero
            data = self._invoice_to_xero_data(
                invoice, xero_currency, xero_account.code
            )
            with transaction.atomic():
                xero = connect()
                InvoiceUUID.objects.init_invoice(invoice)
                uuid = self._api_result(xero.invoices.put(data), "InvoiceID")
            # assign the uuid from Xero to the invoice
            InvoiceUUID.objects.update_uuid(invoice, uuid)

    def sync_date(self, invoice_date):
        """Sync individual (non-batch) invoices.

        .. note:: Previously called ``sync_xero_invoices``.

        """
        count = 0
        invoice_pks = [
            x.pk for x in Invoice.objects.on_account_invoices(invoice_date)
        ]
        logger.info("sync_xero: {} non-batch invoices".format(len(invoice_pks)))
        for pk in invoice_pks:
            invoice = Invoice.objects.get(pk=pk)
            self.sync_invoice(invoice)
            count = count + 1
        return count


class SyncSettings:
    def _sync_accounts_from_xero(self):
        """Sync Xero account codes to our 'Account' table.

        Keys:

        - AccountID
        - Code
        - Name
        - Status
        - Type
        - TaxType
        - Description
        - Class
        - SystemAccount
        - EnablePaymentsToAccount
        - ShowInExpenseClaims
        - BankAccountType
        - ReportingCode
        - HasAttachments
        - UpdatedDateUTC

        """
        count = 0
        xero = connect()
        qs = xero.accounts.all()
        for row in qs:
            count = count + 1
            account_type = row["Type"]
            field_name = "Code"
            if account_type == "BANK":
                field_name = "AccountID"
            code = row[field_name]
            name = row["Name"]
            account_class = row["Class"]
            Account.objects.init_account(code, field_name, name, account_class)
        return count

    def _sync_currencies_from_xero(self):
        """Sync Xero currencies to our '?' table."""
        count = 0
        xero = connect()
        qs = xero.currencies.all()
        for row in qs:
            count = count + 1
            code = row["Code"]
            description = row["Description"]
            XeroCurrency.objects.init_xero_currency(code, description)
        return count

    def _sync_vat_rates_from_xero(self):
        """Sync Xero Tax rates to our 'XeroVatCode' model."""
        count = 0
        xero = connect()
        qs = xero.taxrates.all()
        for row in qs:
            count = count + 1
            code = row["TaxType"]
            name = row["Name"]
            rate = row["EffectiveRate"]
            can_apply_to_liabilities = row["CanApplyToLiabilities"]
            XeroVatCode.objects.init_xero_vat_code(
                code, name, rate, can_apply_to_liabilities
            )
        return count

    def sync(self):
        count = self._sync_accounts_from_xero()
        count = count + self._sync_currencies_from_xero()
        count = count + self._sync_vat_rates_from_xero()
        return count


class ModelUUIDManager(models.Manager):
    def _message(self, instance):
        return "{} ({}) {}".format(
            instance.__class__.__name__, instance.pk, str(instance)
        )

    def _check_sync(self, instance):
        if not instance.is_synced:
            raise XeroError(
                "{} failed sync to Xero previously".format(
                    self._message(instance)
                )
            )

    def _update_uuid(self, instance, uuid):
        if instance.is_synced:
            raise XeroError(
                "{} has already been synced to Xero".format(
                    self._message(instance)
                )
            )
        else:
            instance.uuid = uuid
            instance.save()


class ModelUUID(TimeStampedModel):
    uuid = models.UUIDField(unique=True, blank=True, null=True)

    class Meta:
        abstract = True

    @property
    def is_synced(self):
        return bool(self.uuid)

    def state_as_str(self, instance_as_str):
        if self.is_synced:
            return "{}: {}".format(instance_as_str, self.uuid)
        else:
            return "{} (not synced)".format(instance_as_str)

    def uuid_as_str(self):
        return str(self.uuid)


class BatchUUIDManager(ModelUUIDManager):
    def init_batch(self, batch):
        self.model(batch=batch).save()

    def get_if_exists(self, batch):
        """Get the record if it exists and has been synced.

        .. note:: This will raise an error if the record was initialised but not
                  synced to Xero.

        """
        result = None
        try:
            result = self.model.objects.get(batch=batch)
            self._check_sync(result)
        except self.model.DoesNotExist:
            pass
        return result

    def update_uuid(self, batch, uuid):
        try:
            x = self.model.objects.get(batch=batch)
            self._update_uuid(x, uuid)
        except self.model.DoesNotExist:
            raise XeroError("You must 'init_batch' before you 'update_uuid'")


class BatchUUID(ModelUUID):
    batch = models.OneToOneField(Batch, on_delete=models.CASCADE)
    objects = BatchUUIDManager()

    class Meta:
        verbose_name = "Batch UUID"
        verbose_name_plural = "Batch UUID"

    def __str__(self):
        result = "Batch {} for {}".format(
            self.batch.pk, self.batch.batch_date.strftime("%d/%m/%Y")
        )
        return self.state_as_str(result)


class BatchPaymentUUID(ModelUUID):
    batch = models.OneToOneField(Batch, on_delete=models.CASCADE)
    objects = BatchUUIDManager()

    class Meta:
        verbose_name = "Batch Payment UUID"
        verbose_name_plural = "Batch Payment UUID"

    def __str__(self):
        result = "Batch Payment {} for {}".format(
            self.batch.pk, self.batch.batch_date.strftime("%d/%m/%Y")
        )
        return self.state_as_str(result)


class ContactUUIDManager(ModelUUIDManager):
    def init_contact(self, contact):
        self.model(contact=contact).save()

    def get_if_exists(self, contact):
        """Get the record if it exists and has been synced.

        .. note:: This will raise an error if the record was initialised but not
                  synced to Xero.

        """
        result = None
        try:
            result = self.model.objects.get(contact=contact)
            self._check_sync(result)
        except self.model.DoesNotExist:
            pass
        return result

    def update_uuid(self, contact, uuid):
        try:
            x = self.model.objects.get(contact=contact)
            self._update_uuid(x, uuid)
        except self.model.DoesNotExist:
            raise XeroError("You must 'init_contact' before you 'update_uuid'")


class ContactUUID(ModelUUID):
    contact = models.OneToOneField(Contact, on_delete=models.CASCADE)
    objects = ContactUUIDManager()

    class Meta:
        verbose_name = "Contact UUID"
        verbose_name_plural = "Contact UUID"

    def __str__(self):
        result = "{} (primary key {})".format(
            self.contact.get_full_name(), self.contact.pk
        )
        return self.state_as_str(result)


class InvoiceUUIDManager(ModelUUIDManager):
    def init_invoice(self, invoice):
        self.model(invoice=invoice).save()

    def get_if_exists(self, invoice):
        """Get the record if it exists and has been synced.

        .. note:: This will raise an error if the record was initialised but not
                  synced to Xero.

        """
        result = None
        try:
            result = self.model.objects.get(invoice=invoice)
            self._check_sync(result)
        except self.model.DoesNotExist:
            pass
        return result

    def update_uuid(self, invoice, uuid):
        try:
            x = self.model.objects.get(invoice=invoice)
            self._update_uuid(x, uuid)
        except self.model.DoesNotExist:
            raise XeroError("You must 'init_invoice' before you 'update_uuid'")


class InvoiceUUID(ModelUUID):
    invoice = models.OneToOneField(Invoice, on_delete=models.CASCADE)
    objects = InvoiceUUIDManager()

    class Meta:
        verbose_name = "Invoice UUID"
        verbose_name_plural = "Invoice UUID"

    def __str__(self):
        result = "Invoice {} (primary key {}) for {}".format(
            self.invoice.invoice_number,
            self.invoice.pk,
            self.invoice.contact.get_full_name(),
        )
        return self.state_as_str(result)


class PaymentUUID(ModelUUID):
    invoice = models.OneToOneField(Invoice, on_delete=models.CASCADE)
    objects = InvoiceUUIDManager()

    class Meta:
        verbose_name = "Payment UUID"
        verbose_name_plural = "Payment UUID"

    def __str__(self):
        result = "Payment for invoice {} (primary key {}) for {}".format(
            self.invoice.invoice_number,
            self.invoice.pk,
            self.invoice.contact.get_full_name(),
        )
        return self.state_as_str(result)


class AccountManager(models.Manager):
    def _create_account(self, code, field_name, name, account_class):
        x = self.model(
            code=code,
            field_name=field_name,
            name=name,
            account_class=account_class,
        )
        x.save()
        return x

    def init_account(self, code, field_name, name, account_class):
        name = name[:150]
        try:
            x = self.model.objects.get(code=code)
            x.field_name = field_name
            x.account_class = account_class
            x.name = name
            x.save()
        except self.model.DoesNotExist:
            x = self._create_account(code, field_name, name, account_class)
        return x


class Account(models.Model):
    """A Xero account (like a nominal code)."""

    code = models.CharField(max_length=50, unique=True)
    field_name = models.CharField(max_length=30)
    name = models.CharField(max_length=150)
    account_class = models.SlugField(max_length=10)
    objects = AccountManager()

    class Meta:
        ordering = ("account_class", "code")
        verbose_name = "Xero Account"
        verbose_name_plural = "Xero Accounts"

    def __str__(self):
        result = "{}/{} ({})".format(
            self.code, self.field_name, self.account_class.lower().capitalize()
        )
        if self.name:
            result = "{} {}".format(result, self.name)
        return result

    def account_for_xero(self):
        return {self.field_name: self.code}


class BatchAccountManager(models.Manager):
    def _create_batch_account(self, currency, payment_processor):
        """The contact will be used for posting batches to Xero."""
        user = Contact.objects.create_unique_user(
            currency.slug.lower(), slugify(payment_processor.description)
        )
        contact = Contact.objects.create_contact(user=user)
        x = self.model(
            currency=currency,
            payment_processor=payment_processor,
            contact=contact,
        )
        x.save()
        return x

    def contact_for_batch(self, batch):
        batch_account = self.model.objects.get(
            currency=batch.currency, payment_processor=batch.payment_processor
        )
        return batch_account.contact

    def create_batches(self, batch_date):
        count = 0
        batch_account_pks = [x.pk for x in self.model.objects.all()]
        for pk in batch_account_pks:
            batch_account = self.model.objects.get(pk=pk)
            # create a separate batch for each exchange rate
            for exchange_rate in batch_account.exchange_rates(batch_date):
                try:
                    Batch.objects.get(
                        batch_date=batch_date,
                        currency=batch_account.currency,
                        exchange_rate=exchange_rate,
                        payment_processor=batch_account.payment_processor,
                    )
                except Batch.DoesNotExist:
                    if Batch.objects.create_batch(
                        batch_date,
                        batch_account.currency,
                        exchange_rate,
                        batch_account.payment_processor,
                    ):
                        count = count + 1
        return count

    def init_batch_account(self, invoice_date):
        """Create a record for each currency / processor combination.

        Batch accounts are used to consolidate postings for a collection of
        invoices.  To date, they have only been used for batches where the
        invoices are fully paid at order e.g. online shopping.

        """
        qs = Invoice.objects.current(invoice_date=invoice_date).filter(
            is_credit=False, upfront_payment=True
        )
        for invoice in qs:
            try:
                self.model.objects.get(
                    currency=invoice.currency,
                    payment_processor=invoice.upfront_payment_processor,
                )
            except self.model.DoesNotExist:
                self._create_batch_account(
                    invoice.currency, invoice.upfront_payment_processor
                )


class BatchAccount(models.Model):
    """Which Xero account code to use for postings to Xero.

    The ``Batch`` model in the ``invoice`` app groups invoices by currency and
    payment processor.  This model uses the same two fields and links to the
    nominal code from Xero.

    The code for posting invoices will create ``BatchAccount`` records with
    empty ``invoice_account`` and ``payment_account``.

    To set-up the accounts, browse to *Settings*, *Xero*, *Accounts*.

    """

    currency = models.ForeignKey(
        Currency, on_delete=models.CASCADE, related_name="+"
    )
    payment_processor = models.ForeignKey(
        PaymentProcessor, on_delete=models.CASCADE, related_name="+"
    )
    contact = models.ForeignKey(
        Contact,
        help_text="Post batch invoices to this contact on Xero",
        on_delete=models.CASCADE,
        related_name="+",
    )
    invoice_account = models.ForeignKey(
        Account,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        related_name="+",
    )
    payment_account = models.ForeignKey(
        Account,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        related_name="+",
    )
    objects = BatchAccountManager()

    class Meta:
        ordering = ("currency__slug", "payment_processor")
        unique_together = ("currency", "payment_processor")
        verbose_name = "Batch Account"
        verbose_name_plural = "Batch Accounts"

    def __str__(self):
        code = "Invoice: {}, Payment: {}".format(
            self.invoice_account.code if self.invoice_account else "<Pending>",
            self.payment_account.code if self.payment_account else "<Pending>",
        )
        return "{}: {} for '{}'".format(
            code, self.currency.slug, self.payment_processor.description
        )

    def exchange_rates(self, invoice_date):
        """A set of the exchange rates for invoices on this date."""
        return Invoice.objects.exchange_rates(
            invoice_date, self.currency, self.payment_processor
        )


reversion.register(BatchAccount)


class XeroVatCodeManager(models.Manager):
    def _create_xero_vat_code(self, code, name, rate, can_apply_to_liabilities):
        x = self.model(
            code=code,
            name=name,
            rate=rate,
            can_apply_to_liabilities=can_apply_to_liabilities,
        )
        x.save()
        return x

    def init_xero_vat_code(self, code, name, rate, can_apply_to_liabilities):
        try:
            x = self.model.objects.get(code=code)
            x.name = name
            x.rate = rate
            x.can_apply_to_liabilities = can_apply_to_liabilities
            x.save()
        except self.model.DoesNotExist:
            x = self._create_xero_vat_code(
                code, name, rate, can_apply_to_liabilities
            )
        return x

    def vat_code_for(self, vat_code_slug):
        try:
            result = self.model.objects.get(vat_code__slug=vat_code_slug)
            return result.code
        except self.model.DoesNotExist:
            raise XeroError(
                "VAT code '{}' not set-up in Xero VAT settings".format(
                    vat_code_slug
                )
            )


class XeroVatCode(models.Model):
    """A Xero VAT code."""

    code = models.SlugField(max_length=20, unique=True)
    name = models.CharField(max_length=100)
    rate = models.DecimalField(max_digits=8, decimal_places=2)
    can_apply_to_liabilities = models.BooleanField()
    vat_code = models.ForeignKey(
        VatCode,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        related_name="+",
    )
    objects = XeroVatCodeManager()

    class Meta:
        ordering = ("vat_code__slug", "code")
        verbose_name = "Xero VAT Code"
        verbose_name_plural = "Xero VAT Codes"

    def __str__(self):
        return "{}, {} @ {}%".format(self.code, self.name, self.rate)


reversion.register(XeroVatCode)


class XeroCurrencyManager(models.Manager):
    def _create_xero_currency(self, code, description):
        x = self.model(code=code, description=description)
        x.save()
        return x

    def init_xero_currency(self, code, code_description):
        description = code_description[:100]
        try:
            x = self.model.objects.get(code=code)
            x.description = description
            x.save()
        except self.model.DoesNotExist:
            x = self._create_xero_currency(code, description)
        return x


class XeroCurrency(models.Model):
    """A Xero currency."""

    code = models.SlugField(max_length=10, unique=True)
    description = models.CharField(max_length=100)
    currency = models.ForeignKey(
        Currency,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        related_name="+",
    )
    objects = XeroCurrencyManager()

    class Meta:
        ordering = ("code",)
        verbose_name = "Xero Currency"
        verbose_name_plural = "Xero Currencies"

    def __str__(self):
        result = self.code
        if self.description:
            result = "{} ({})".format(result, self.description)
        if self.currency:
            result = "{} {}".format(result, self.currency.slug)
        return result

    def is_base_currency(self):
        xero_settings = XeroSettings.load()
        if not xero_settings.base_currency:
            raise XeroError(
                "Xero settings have not been updated. "
                "Please set the base currency."
            )
        return bool(self.currency == xero_settings.base_currency)


reversion.register(XeroCurrency)


class XeroSettings(SingletonModel):
    days_to_sync = models.IntegerField(
        help_text="How many days (before today) to sync"
    )
    invoice_account = models.ForeignKey(
        Account,
        help_text="Xero account for posting individual invoices",
        on_delete=models.CASCADE,
        related_name="+",
    )
    base_currency = models.ForeignKey(
        Currency,
        help_text="Base currency for Xero postings",
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        related_name="+",
    )

    class Meta:
        verbose_name = "Xero settings"

    def __str__(self):
        return "Days to Sync {}, Invoice account '{}'".format(
            self.days_to_sync, self.invoice_account.code
        )


reversion.register(XeroSettings)
