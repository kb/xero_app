# -*- encoding: utf-8 -*-
import dramatiq
import logging

from django.conf import settings

from invoice.models import Batch, BatchInvoice, Invoice
from .models import SyncBatch, SyncCredit, SyncInvoice, SyncSettings


logger = logging.getLogger(__name__)


# https://dramatiq.io/cookbook.html#binding-worker-groups-to-queues
@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME)
def xero_sync_batch(batch_pk):
    """Sync a batch.

    .. note:: Used by superuser views to force a sync.

    """
    batch = Batch.objects.get(pk=batch_pk)
    logger.info(
        "Sync batch '{}' ({}) to Xero".format(batch.batch_number, batch.pk)
    )
    sync_batch = SyncBatch()
    sync_batch.sync(batch)
    logger.info(
        "Sync batch '{}' ({}) to Xero - Complete".format(
            batch.batch_number, batch.pk
        )
    )
    return batch.pk


# https://dramatiq.io/cookbook.html#binding-worker-groups-to-queues
@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME)
def xero_sync_batch_payment(batch_pk):
    """Sync a batch payment.

    .. note:: Used by superuser views to force a sync.

    """
    batch = Batch.objects.get(pk=batch_pk)
    logger.info(
        "Sync batch payment '{}' ({}) to Xero".format(
            batch.batch_number, batch.pk
        )
    )
    sync_batch = SyncBatch()
    sync_batch.sync_payment(batch)
    logger.info(
        "Sync batch payment '{}' ({}) to Xero - Complete".format(
            batch.batch_number, batch.pk
        )
    )
    return batch.pk


# https://dramatiq.io/cookbook.html#binding-worker-groups-to-queues
@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME)
def xero_sync_invoice(invoice_pk):
    """Sync an invoice.

    .. note:: Used by superuser views to force a sync.

    """
    invoice = Invoice.objects.get(pk=invoice_pk)
    message = "credit note" if invoice.is_credit else "invoice"
    logger.info(
        "Sync {} '{}' ({}) to Xero".format(
            message, invoice.invoice_number, invoice.pk
        )
    )
    if invoice.is_credit:
        sync_credit = SyncCredit()
        sync_credit.sync_credit_note(invoice)
    else:
        sync_invoice = SyncInvoice()
        sync_invoice.sync_invoice(invoice)
    logger.info(
        "Sync {} '{}' ({}) to Xero - Complete".format(
            message, invoice.invoice_number, invoice.pk
        )
    )
    return invoice.pk


# https://dramatiq.io/cookbook.html#binding-worker-groups-to-queues
@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME)
def xero_sync_payment(invoice_pk):
    """Sync an invoice payment.

    .. note:: Used by superuser views to force a sync.

    """
    invoice = Invoice.objects.get(pk=invoice_pk)
    message = "credit note" if invoice.is_credit else "invoice"
    logger.info(
        "Sync {} '{}' ({}) to Xero".format(
            message, invoice.invoice_number, invoice.pk
        )
    )
    if invoice.is_credit:
        sync_credit = SyncCredit()
        sync_credit.sync_refund(invoice)
    else:
        raise NotImplementedError("Need to sync invoice payment to Xero")
    logger.info(
        "Sync {} '{}' ({}) to Xero - Complete".format(
            message, invoice.invoice_number, invoice.pk
        )
    )
    return invoice.pk


# https://dramatiq.io/cookbook.html#binding-worker-groups-to-queues
@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME)
def xero_sync_settings():
    count = 0
    logger.info("Sync Xero Settings (accounts and currencies)...")
    sync_settings = SyncSettings()
    count = sync_settings.sync()
    logger.info("Sync Xero Settings ({} records) - Complete".format(count))
    return count
