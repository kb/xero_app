# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from contact.models import Contact
from xero_app.models import sync_contact_to_xero


class Command(BaseCommand):

    help = "Sync contact to Xero"

    def add_arguments(self, parser):
        parser.add_argument("contact_pk", nargs="?", type=int)

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        contact_pk = options["contact_pk"]
        contact = Contact.objects.get(pk=contact_pk)
        self.stdout.write("Sync '{}' ...".format(contact.get_full_name))
        sync_contact_to_xero(contact)
        self.stdout.write("{} - Complete".format(self.help))
