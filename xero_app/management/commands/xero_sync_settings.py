# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from xero_app.tasks import xero_sync_settings


class Command(BaseCommand):

    help = "Sync Xero Settings"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        count = xero_sync_settings()
        self.stdout.write("{} ({} records) - Complete".format(self.help, count))
