# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from invoice.models import BatchInvoice, Invoice, InvoiceIssue
from xero_app.models import BatchUUID, InvoiceUUID


class Command(BaseCommand):

    help = "Check all transactions posted to Xero"

    def handle(self, *args, **options):
        self.stdout.write(self.help)
        invoices = set([x.pk for x in Invoice.objects.all()])
        self.stdout.write("{} invoices".format(len(invoices)))
        # pks for all the batches and invoices within a batch
        batches = set()
        batch_invoices = set()
        for x in BatchInvoice.objects.all():
            batch_invoices.add(x.invoice.pk)
            batches.add(x.batch.pk)
        self.stdout.write("{} batches in 'BatchInvoice'".format(len(batches)))
        self.stdout.write(
            "{} invoices in 'BatchInvoice'".format(len(batch_invoices))
        )
        # pks for batch UUIDs
        batch_uuids = set([x.batch.pk for x in BatchUUID.objects.all()])
        self.stdout.write("{} batches in 'BatchUUID'".format(len(batch_uuids)))
        # pks for invoice UUIDs
        invoice_uuids = set([x.invoice.pk for x in InvoiceUUID.objects.all()])
        self.stdout.write(
            "{} invoices in 'InvoiceUUID'".format(len(invoice_uuids))
        )
        # pks for invoice issues
        invoice_issues = set([x.invoice.pk for x in InvoiceIssue.objects.all()])
        self.stdout.write(
            "{} invoices in 'InvoiceIssue'".format(len(invoice_issues))
        )
        # check #1 - make sure all invoices and credit notes were posted
        self.stdout.write(
            "Total {} invoices == {} (differences {})".format(
                len(invoices),
                len(batch_invoices | invoice_uuids | invoice_issues),
                invoices.difference(
                    batch_invoices | invoice_uuids | invoice_issues
                ),
            )
        )
        # check #2 - make sure all batches were posted
        self.stdout.write(
            "Total {} batches == {} (differences {})".format(
                len(batches),
                len(batch_uuids),
                batches.difference(batch_uuids),
            )
        )
        self.stdout.write("{} - Complete".format(self.help))
