# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError

from invoice.models import Invoice
from xero_app.tasks import xero_sync_invoice


class Command(BaseCommand):
    help = "Sync invoice or credit note to Xero"

    def add_arguments(self, parser):
        parser.add_argument("invoice_pk", type=int)

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        invoice_pk = options["invoice_pk"]
        invoice = Invoice.objects.get(pk=invoice_pk)
        self.stdout.write(
            "Sync invoice or credit note number '{}' for {} "
            "(primary key {})".format(
                invoice.invoice_number,
                invoice.contact.get_full_name,
                invoice.pk,
            )
        )
        xero_sync_invoice(invoice.pk)
        self.stdout.write("{} - Complete".format(self.help))
