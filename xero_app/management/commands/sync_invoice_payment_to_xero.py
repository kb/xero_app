# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from invoice.models import Invoice
from xero_app.models import sync_invoice_payment_to_xero


class Command(BaseCommand):

    help = "Sync invoice payment to Xero"

    def add_arguments(self, parser):
        parser.add_argument("invoice_pk", nargs="?", type=int)

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        invoice_pk = options["invoice_pk"]
        invoice = Invoice.objects.get(pk=invoice_pk)
        # print(invoice.vat_analysis())
        self.stdout.write(
            "Sync payment for invoice number '{}' for "
            "{} (primary key {})".format(
                invoice.number, invoice.contact.get_full_name, invoice.pk
            )
        )
        sync_invoice_payment_to_xero(invoice)
        self.stdout.write("{} - Complete".format(self.help))
