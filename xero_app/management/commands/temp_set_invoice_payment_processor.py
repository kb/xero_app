# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from invoice.models import Currency, Invoice, PaymentProcessor
from xero_app.models import BatchAccount


class Command(BaseCommand):

    help = "Add payment processor to all invoices (for testing) #3973"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        currency = Currency.objects.get(slug=Currency.POUND)
        payment_processor = PaymentProcessor.objects.init_payment_processor(
            "stripe", "Stripe"
        )
        BatchAccount.objects.init_batch_account(currency, payment_processor)
        count = Invoice.objects.all().update(
            upfront_payment=True, upfront_payment_processor=payment_processor
        )
        self.stdout.write("Updated {} invoices".format(count))
        self.stdout.write("{} - Complete".format(self.help))
