# -*- encoding: utf-8 -*-
import factory
import uuid

from decimal import Decimal

from contact.tests.factories import ContactFactory
from finance.tests.factories import CurrencyFactory
from invoice.tests.factories import (
    BatchFactory,
    InvoiceFactory,
    PaymentProcessorFactory,
)
from xero_app.models import (
    Account,
    BatchAccount,
    BatchPaymentUUID,
    BatchUUID,
    ContactUUID,
    InvoiceUUID,
    PaymentUUID,
    XeroCurrency,
    XeroSettings,
    XeroVatCode,
)


class AccountFactory(factory.django.DjangoModelFactory):
    account_class = "REVENUE"

    class Meta:
        model = Account

    @factory.sequence
    def code(n):
        return n + 1

    @factory.sequence
    def name(n):
        return "name_{}".format(n)


class BatchAccountFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = BatchAccount

    currency = factory.SubFactory(CurrencyFactory)
    payment_processor = factory.SubFactory(PaymentProcessorFactory)
    contact = factory.SubFactory(ContactFactory)
    invoice_account = factory.SubFactory(AccountFactory)
    payment_account = factory.SubFactory(AccountFactory)


class BatchPaymentUUIDFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = BatchPaymentUUID

    batch = factory.SubFactory(BatchFactory)

    @factory.lazy_attribute
    def uuid(self):
        return uuid.uuid4()


class BatchUUIDFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = BatchUUID

    batch = factory.SubFactory(BatchFactory)

    @factory.lazy_attribute
    def uuid(self):
        return uuid.uuid4()


class ContactUUIDFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ContactUUID

    contact = factory.SubFactory(ContactFactory)

    @factory.lazy_attribute
    def uuid(self):
        return uuid.uuid4()


class InvoiceUUIDFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = InvoiceUUID

    invoice = factory.SubFactory(InvoiceFactory)

    @factory.lazy_attribute
    def uuid(self):
        return uuid.uuid4()


class PaymentUUIDFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = PaymentUUID

    invoice = factory.SubFactory(InvoiceFactory)

    @factory.lazy_attribute
    def uuid(self):
        return uuid.uuid4()


class XeroCurrencyFactory(factory.django.DjangoModelFactory):
    currency = factory.SubFactory(CurrencyFactory)

    class Meta:
        model = XeroCurrency

    @factory.sequence
    def code(n):
        return "code_{}".format(n)

    @factory.sequence
    def description(n):
        return "description_{}".format(n)


class XeroSettingsFactory(factory.django.DjangoModelFactory):
    days_to_sync = 7

    class Meta:
        model = XeroSettings

    invoice_account = factory.SubFactory(AccountFactory)


class XeroVatCodeFactory(factory.django.DjangoModelFactory):
    can_apply_to_liabilities = True

    class Meta:
        model = XeroVatCode

    @factory.sequence
    def code(n):
        return "code_{}".format(n)

    @factory.sequence
    def name(n):
        return "name_{}".format(n)

    @factory.sequence
    def rate(n):
        return Decimal(n)
