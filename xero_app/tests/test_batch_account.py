# -*- encoding: utf-8 -*-
import pytest

from datetime import date
from decimal import Decimal
from django.db import IntegrityError

from contact.tests.factories import ContactFactory
from finance.models import Currency
from invoice.models import Batch
from invoice.tests.factories import (
    BatchFactory,
    CurrencyFactory,
    InvoiceFactory,
    PaymentProcessorFactory,
)
from xero_app.models import BatchAccount
from .factories import BatchAccountFactory, AccountFactory


@pytest.mark.django_db
def test_contact_for_batch():
    contact = ContactFactory()
    currency = Currency.objects.get(slug="GBP")
    payment_processor = PaymentProcessorFactory(description="paypal")
    BatchAccountFactory(
        contact=contact, currency=currency, payment_processor=payment_processor
    )
    batch = BatchFactory(currency=currency, payment_processor=payment_processor)
    assert contact == BatchAccount.objects.contact_for_batch(batch)


@pytest.mark.django_db
def test_create_batches():
    batch_date = date(2018, 2, 22)
    currency = Currency.objects.get(slug="GBP")
    payment_processor = PaymentProcessorFactory(description="paypal")
    BatchAccountFactory(currency=currency, payment_processor=payment_processor)
    invoice_1 = InvoiceFactory(
        invoice_date=batch_date,
        currency=currency,
        upfront_payment=True,
        upfront_payment_processor=payment_processor,
    )
    InvoiceFactory(
        invoice_date=date(2018, 2, 21),
        currency=currency,
        upfront_payment=True,
        upfront_payment_processor=payment_processor,
    )
    invoice_3 = InvoiceFactory(
        invoice_date=batch_date,
        currency=currency,
        upfront_payment=True,
        upfront_payment_processor=payment_processor,
    )
    assert 0 == Batch.objects.count()
    assert 1 == BatchAccount.objects.create_batches(batch_date)
    assert 1 == Batch.objects.count()
    batch = Batch.objects.first()
    assert set([invoice_1.pk, invoice_3.pk]) == set(
        [x.invoice.pk for x in batch.invoices()]
    )


@pytest.mark.django_db
def test_exchange_rates():
    currency = Currency.objects.get(slug=Currency.EURO)
    payment_processor = PaymentProcessorFactory()
    invoice_account = AccountFactory()
    invoice_date = date.today()
    payment_account = AccountFactory()
    batch_account = BatchAccountFactory(
        currency=currency,
        payment_processor=payment_processor,
        invoice_account=invoice_account,
        payment_account=payment_account,
    )
    InvoiceFactory(
        invoice_date=invoice_date,
        currency=currency,
        exchange_rate=Decimal(),
        upfront_payment=True,
        upfront_payment_processor=payment_processor,
    )
    InvoiceFactory(
        currency=currency,
        invoice_date=invoice_date,
        exchange_rate=Decimal("0.999"),
        upfront_payment=True,
        upfront_payment_processor=PaymentProcessorFactory(),
    )
    InvoiceFactory(
        currency=currency,
        invoice_date=invoice_date,
        exchange_rate=Decimal("0.888"),
        upfront_payment=True,
        upfront_payment_processor=payment_processor,
    )
    InvoiceFactory(
        currency=Currency.objects.get(slug=Currency.POUND),
        invoice_date=invoice_date,
        exchange_rate=Decimal("0.777"),
        upfront_payment=True,
        upfront_payment_processor=payment_processor,
    )
    InvoiceFactory(
        currency=currency,
        invoice_date=invoice_date,
        exchange_rate=Decimal("0.888"),
        upfront_payment=True,
        upfront_payment_processor=payment_processor,
    )
    InvoiceFactory(
        currency=currency,
        invoice_date=invoice_date,
        exchange_rate=Decimal("0.555"),
        upfront_payment=False,
        upfront_payment_processor=payment_processor,
    )
    assert set([Decimal(), Decimal("0.888")]) == batch_account.exchange_rates(
        invoice_date
    )


@pytest.mark.django_db
def test_init_batch_account():
    currency = Currency.objects.get(slug=Currency.POUND)
    payment_processor = PaymentProcessorFactory(description="Stripe")
    invoice = InvoiceFactory(
        currency=currency,
        upfront_payment=True,
        upfront_payment_processor=payment_processor,
    )
    assert 0 == BatchAccount.objects.count()
    BatchAccount.objects.init_batch_account(invoice.invoice_date)
    assert 1 == BatchAccount.objects.count()
    batch_account = BatchAccount.objects.first()
    assert currency == batch_account.currency
    assert payment_processor == batch_account.payment_processor
    assert "gbp.stripe" == batch_account.contact.user.username
    assert batch_account.invoice_account is None
    assert batch_account.payment_account is None


@pytest.mark.django_db
def test_init_batch_account_exists():
    currency = CurrencyFactory()
    payment_processor = PaymentProcessorFactory()
    invoice_account = AccountFactory()
    payment_account = AccountFactory()
    BatchAccountFactory(
        currency=currency,
        payment_processor=payment_processor,
        invoice_account=invoice_account,
        payment_account=payment_account,
    )
    assert 1 == BatchAccount.objects.count()
    invoice = InvoiceFactory(
        currency=currency,
        upfront_payment=True,
        upfront_payment_processor=payment_processor,
    )
    BatchAccount.objects.init_batch_account(invoice.invoice_date)
    assert 1 == BatchAccount.objects.count()
    batch_account = BatchAccount.objects.first()
    assert currency == batch_account.currency
    assert payment_processor == batch_account.payment_processor
    assert invoice_account == batch_account.invoice_account
    assert payment_account == batch_account.payment_account


@pytest.mark.django_db
def test_ordering():
    currency_b = CurrencyFactory(slug="b")
    BatchAccountFactory(
        currency=currency_b,
        invoice_account=AccountFactory(code=1),
        payment_processor=PaymentProcessorFactory(description="x"),
    )
    BatchAccountFactory(
        currency=currency_b,
        invoice_account=AccountFactory(code=2),
        payment_processor=PaymentProcessorFactory(description="y"),
    )
    BatchAccountFactory(
        currency=CurrencyFactory(slug="a"),
        invoice_account=AccountFactory(code=3),
        payment_processor=PaymentProcessorFactory(description="a"),
    )
    assert ["3", "1", "2"] == [
        x.invoice_account.code for x in BatchAccount.objects.all()
    ]


@pytest.mark.django_db
def test_unique_together():
    currency = CurrencyFactory()
    payment_processor = PaymentProcessorFactory()
    BatchAccountFactory(currency=currency, payment_processor=payment_processor)
    with pytest.raises(IntegrityError):
        BatchAccountFactory(
            currency=currency, payment_processor=payment_processor
        )


@pytest.mark.django_db
def test_str():
    currency = CurrencyFactory(slug="XYZ")
    payment_processor = PaymentProcessorFactory(description="stripe")
    invoice_account = AccountFactory(code=123)
    payment_account = AccountFactory(code=456)
    batch_account = BatchAccountFactory(
        currency=currency,
        payment_processor=payment_processor,
        invoice_account=invoice_account,
        payment_account=payment_account,
    )
    assert "Invoice: 123, Payment: 456: XYZ for 'stripe'" == str(batch_account)


@pytest.mark.django_db
def test_str_no_account():
    currency = CurrencyFactory(slug="XYZ")
    payment_processor = PaymentProcessorFactory(description="stripe")
    batch_account = BatchAccountFactory(
        currency=currency,
        payment_processor=payment_processor,
        invoice_account=None,
        payment_account=None,
    )
    assert "Invoice: <Pending>, Payment: <Pending>: XYZ for 'stripe'" == str(
        batch_account
    )
