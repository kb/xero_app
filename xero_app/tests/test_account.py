# -*- encoding: utf-8 -*-
import pytest

from xero_app.models import Account
from .factories import AccountFactory


@pytest.mark.django_db
def test_account_for_xero():
    account = AccountFactory(code="100", field_name="Code")
    assert {"Code": "100"} == account.account_for_xero()


@pytest.mark.django_db
def test_account_for_xero_account_id():
    account = AccountFactory(code="100", field_name="AccountID")
    assert {"AccountID": "100"} == account.account_for_xero()


@pytest.mark.django_db
def test_init_account():
    account = Account.objects.init_account("100", "Code", "Apple", "REVENUE")
    assert "100" == account.code
    assert "Apple" == account.name
    assert "REVENUE" == account.account_class


@pytest.mark.django_db
def test_init_account_existing():
    account = AccountFactory(
        code="0101", field_name="Code", name="Apple", account_class="REVENUE"
    )
    account = Account.objects.init_account(
        "0101", "Code", "Apple Crumble", "LIABILITY"
    )
    assert 1 == Account.objects.count()
    account = Account.objects.first()
    assert "0101" == account.code
    assert "Apple Crumble" == account.name
    assert "LIABILITY" == account.account_class


@pytest.mark.django_db
def test_init_account_long_description():
    description = (
        "A1234567890123456789012345678901234567890123456789"
        "B1234567890123456789012345678901234567890123456789"
        "C1234567890123456789012345678901234567890123456789"
        "D1234567890123456789012345678901234567890123456789"
    )
    account = Account.objects.init_account(
        "100", "Code", description, "REVENUE"
    )
    assert "100" == account.code
    assert (
        "A1234567890123456789012345678901234567890123456789"
        "B1234567890123456789012345678901234567890123456789"
        "C1234567890123456789012345678901234567890123456789"
    ) == account.name
    assert "REVENUE" == account.account_class


@pytest.mark.django_db
def test_ordering():
    AccountFactory(code=1, account_class="REVENUE")
    AccountFactory(code=2, account_class="REVENUE")
    AccountFactory(code=3, account_class="LIABILITY")
    assert ["3", "1", "2"] == [x.code for x in Account.objects.all()]


@pytest.mark.parametrize(
    "code,field_name,name,account_class,expect",
    [
        (123, "Code", "Apple", "REVENUE", "123/Code (Revenue) Apple"),
        (345, "AccountID", "", "LIABILITY", "345/AccountID (Liability)"),
    ],
)
@pytest.mark.django_db
def test_str(code, field_name, name, account_class, expect):
    assert expect == str(
        AccountFactory(
            code=code,
            field_name=field_name,
            name=name,
            account_class=account_class,
        )
    )
