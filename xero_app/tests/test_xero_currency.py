# -*- encoding: utf-8 -*-
import pytest

from finance.models import Currency
from finance.tests.factories import CurrencyFactory
from xero_app.models import XeroCurrency, XeroError
from .factories import AccountFactory, XeroCurrencyFactory, XeroSettingsFactory


@pytest.mark.django_db
def test_init_xero_currency():
    currency = XeroCurrency.objects.init_xero_currency("A", "Apple")
    assert "A" == currency.code
    assert "Apple" == currency.description
    assert currency.currency is None


@pytest.mark.django_db
def test_init_xero_currency_existing():
    currency = CurrencyFactory()
    xero_currency = XeroCurrencyFactory(
        code="A", description="Apple", currency=currency
    )
    xero_currency = XeroCurrency.objects.init_xero_currency(
        "A", "Apple Crumble"
    )
    assert 1 == XeroCurrency.objects.count()
    xero_currency = XeroCurrency.objects.first()
    assert "A" == xero_currency.code
    assert "Apple Crumble" == xero_currency.description
    assert currency == xero_currency.currency


@pytest.mark.django_db
def test_init_xero_currency_long_description():
    description = (
        "A1234567890123456789012345678901234567890123456789"
        "B1234567890123456789012345678901234567890123456789"
        "C1234567890123456789012345678901234567890123456789"
    )
    xero_currency = XeroCurrency.objects.init_xero_currency("A", description)
    assert "A" == xero_currency.code
    assert (
        "A1234567890123456789012345678901234567890123456789"
        "B1234567890123456789012345678901234567890123456789"
    ) == xero_currency.description


@pytest.mark.parametrize(
    "slug,expect",
    [(Currency.POUND, True), (Currency.EURO, False), (Currency.DOLLAR, False)],
)
@pytest.mark.django_db
def test_is_base_currency(slug, expect):
    XeroSettingsFactory(
        days_to_sync=6,
        invoice_account=AccountFactory(code="010203"),
        base_currency=Currency.objects.get(slug=Currency.POUND),
    )
    currency = Currency.objects.get(slug=slug)
    xero_currency = XeroCurrencyFactory(
        code="A", description="Apple", currency=currency
    )
    assert xero_currency.is_base_currency() is expect


@pytest.mark.django_db
def test_is_base_currency_no_settings():
    xero_currency = XeroCurrencyFactory()
    with pytest.raises(XeroError) as e:
        xero_currency.is_base_currency()
    assert (
        "Xero settings have not been updated. Please set the base currency."
        in str(e.value)
    )


@pytest.mark.django_db
def test_ordering():
    XeroCurrencyFactory(code="c")
    XeroCurrencyFactory(code="b")
    XeroCurrencyFactory(code="a")
    assert ["a", "b", "c"] == [x.code for x in XeroCurrency.objects.all()]


@pytest.mark.parametrize(
    "code,description,currency_slug,expect",
    [
        ("123", "Apple", "C1", "123 (Apple) C1"),
        ("345", "", "C2", "345 C2"),
        ("678", "Orange", None, "678 (Orange)"),
    ],
)
@pytest.mark.django_db
def test_str(code, description, currency_slug, expect):
    currency = None
    if currency_slug:
        currency = CurrencyFactory(slug=currency_slug)
    assert expect == str(
        XeroCurrencyFactory(
            code=code, description=description, currency=currency
        )
    )
