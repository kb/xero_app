# -*- encoding: utf-8 -*-
import pytest
import uuid

from contact.tests.factories import ContactFactory
from invoice.tests.factories import InvoiceFactory
from login.tests.factories import UserFactory
from xero_app.models import InvoiceUUID, XeroError


@pytest.mark.django_db
def test_invoice_uuid():
    invoice = InvoiceFactory()
    assert InvoiceUUID.objects.get_if_exists(invoice) is None
    x = str(uuid.uuid4())
    # init
    InvoiceUUID.objects.init_invoice(invoice)
    # get_if_exists (should fail before '_update_uuid')
    with pytest.raises(XeroError) as e:
        InvoiceUUID.objects.get_if_exists(invoice)
    assert "failed sync to Xero previously" in str(e.value)
    assert 1 == InvoiceUUID.objects.count()
    invoice_uuid = InvoiceUUID.objects.first()
    # is_synced
    assert invoice_uuid.is_synced is False
    assert 1 == InvoiceUUID.objects.count()
    assert invoice == invoice_uuid.invoice
    # update_uuid
    InvoiceUUID.objects.update_uuid(invoice, x)
    assert 1 == InvoiceUUID.objects.count()
    # get_if_exists
    invoice_uuid = InvoiceUUID.objects.get_if_exists(invoice)
    assert invoice_uuid is not None
    # is_synced
    assert invoice_uuid.is_synced is True
    assert x == str(invoice_uuid.uuid)
    # update_uuid (again)
    with pytest.raises(XeroError) as e:
        InvoiceUUID.objects.update_uuid(invoice, x)
    assert "has already been synced" in str(e.value)


@pytest.mark.django_db
def test_str():
    contact = ContactFactory(
        company_name="", user=UserFactory(first_name="P", last_name="Kimber")
    )
    invoice = InvoiceFactory(contact=contact, number=222)
    # init_invoice
    InvoiceUUID.objects.init_invoice(invoice)
    assert 1 == InvoiceUUID.objects.count()
    invoice_uuid = InvoiceUUID.objects.first()
    assert "Invoice 000222 (primary key {}) for P Kimber (not synced)".format(
        invoice.pk
    ) == str(invoice_uuid)
    # update_uuid
    x = str(uuid.uuid4())
    InvoiceUUID.objects.update_uuid(invoice, x)
    assert 1 == InvoiceUUID.objects.count()
    invoice_uuid = InvoiceUUID.objects.first()
    assert "Invoice 000222 (primary key {}) for P Kimber: {}".format(
        invoice.pk, x
    ) == str(invoice_uuid)
