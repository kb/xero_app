# -*- encoding: utf-8 -*-
import os
import pytest
import responses
import uuid

from datetime import date
from decimal import Decimal
from django.core.management import call_command
from http import HTTPStatus

from contact.tests.factories import ContactFactory
from finance.models import Currency
from invoice.models import InvoiceCredit, InvoiceIssue
from invoice.tests.factories import InvoiceContactFactory, InvoiceFactory
from xero_app.models import InvoiceUUID
from .factories import (
    AccountFactory,
    XeroCurrencyFactory,
    XeroSettingsFactory,
    XeroVatCodeFactory,
)


@pytest.mark.django_db
@responses.activate
def test_xero_sync_invoice(settings):
    """Sync an invoice or credit note to Xero.

    .. note:: This test is posting a credit note for an on-account customer
              to Xero.

    Test setup copied from:
    ``test_sync_date_credit_note_not_allocated_to_a_batch``
    in ``xero_app/tests/test_sync_credit.py``.

    """
    settings.XERO_KEY_FILE = os.path.join(
        os.path.dirname(os.path.realpath(__file__)), "data", "privatekey.pem"
    )
    contact_uuid = str(uuid.uuid4())
    credit_note_uuid = str(uuid.uuid4())
    responses.add(
        responses.PUT,
        "https://api.xero.com/api.xro/2.0/Contacts",
        json={"Status": "OK", "Contacts": [{"ContactID": contact_uuid}]},
        status=HTTPStatus.OK,
    )
    responses.add(
        responses.PUT,
        "https://api.xero.com/api.xro/2.0/CreditNotes",
        json={
            "Status": "OK",
            "CreditNotes": [{"CreditNoteID": credit_note_uuid}],
        },
        status=HTTPStatus.OK,
    )
    base_currency = Currency.objects.get(slug=Currency.POUND)
    XeroSettingsFactory(
        days_to_sync=6,
        invoice_account=AccountFactory(code="010101"),
        base_currency=base_currency,
    )
    XeroCurrencyFactory(code="GBP", currency=base_currency)
    contact = ContactFactory()
    InvoiceContactFactory(contact=contact, on_account=True)
    invoice_date = date(2019, 7, 21)
    invoice = InvoiceFactory(
        contact=contact, invoice_date=invoice_date, is_credit=False
    )
    credit_note = InvoiceFactory(
        contact=contact,
        invoice_date=invoice_date,
        is_credit=True,
        exchange_rate=Decimal("1.000"),
    )
    InvoiceCredit.objects.init_invoice_credit(invoice, credit_note)
    assert 0 == InvoiceUUID.objects.count()
    call_command("xero_sync_invoice", credit_note.pk)
    assert 1 == InvoiceUUID.objects.count()
    invoice_uuid = InvoiceUUID.objects.get(invoice=credit_note)
    assert credit_note_uuid == str(invoice_uuid.uuid)
