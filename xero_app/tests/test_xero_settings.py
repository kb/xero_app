# -*- encoding: utf-8 -*-
import pytest

from .factories import AccountFactory, XeroSettingsFactory


@pytest.mark.django_db
def test_str():
    assert "Days to Sync 6, Invoice account '010203'" == str(
        XeroSettingsFactory(
            days_to_sync=6, invoice_account=AccountFactory(code="010203")
        )
    )
