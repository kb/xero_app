# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from http import HTTPStatus
from unittest import mock

from invoice.tests.factories import (
    BatchFactory,
    CurrencyFactory,
    InvoiceFactory,
    PaymentProcessorFactory,
)
from finance.models import VatCode
from login.tests.factories import TEST_PASSWORD, UserFactory
from xero_app.models import XeroSettings
from .factories import (
    AccountFactory,
    BatchAccountFactory,
    BatchPaymentUUIDFactory,
    BatchUUIDFactory,
    XeroCurrencyFactory,
    XeroVatCodeFactory,
)


@pytest.mark.django_db
def test_batch_account_list(client):
    user = UserFactory(is_staff=True)
    BatchAccountFactory(
        currency=CurrencyFactory(slug="b"),
        invoice_account=AccountFactory(code=2),
        payment_account=AccountFactory(code=3),
    )
    BatchAccountFactory(
        currency=CurrencyFactory(slug="a"),
        invoice_account=AccountFactory(code=1),
        payment_account=AccountFactory(code=4),
    )
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("xero.account.list"))
    # check
    assert HTTPStatus.OK == response.status_code
    assert "object_list" in response.context
    assert ["1", "2"] == [
        x.invoice_account.code for x in response.context["object_list"]
    ]


@pytest.mark.django_db
def test_batch_account_update(client):
    currency = CurrencyFactory(slug="XYZ")
    payment_processor = PaymentProcessorFactory(description="stripe")
    batch_nominal = BatchAccountFactory(
        currency=currency,
        payment_processor=payment_processor,
        invoice_account=AccountFactory(code=123),
        payment_account=AccountFactory(code=456),
    )
    # login
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    invoice_account = AccountFactory(code=818)
    payment_account = AccountFactory(code=323)
    url = reverse("xero.account.update", args=[batch_nominal.pk])
    response = client.post(
        url,
        data={
            "invoice_account": invoice_account.pk,
            "payment_account": payment_account.pk,
        },
    )
    # check
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("xero.account.list") == response.url
    batch_nominal.refresh_from_db()
    assert invoice_account == batch_nominal.invoice_account
    assert payment_account == batch_nominal.payment_account


@pytest.mark.django_db
def test_batch_account_update_missing_account(client):
    currency = CurrencyFactory(slug="XYZ")
    payment_processor = PaymentProcessorFactory(description="stripe")
    batch_nominal = BatchAccountFactory(
        currency=currency, payment_processor=payment_processor
    )
    # login
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("xero.account.update", args=[batch_nominal.pk])
    response = client.post(url)
    # check
    assert HTTPStatus.OK == response.status_code
    form = response.context["form"]
    assert {
        "__all__": ["Please select an invoice and payment account."]
    } == form.errors


@pytest.mark.django_db
def test_xero_batch_detail(client):
    batch = BatchFactory()
    BatchPaymentUUIDFactory(
        batch=batch, uuid="059b59ec-baf0-4e2b-99a7-c6ad83addd92"
    )
    BatchUUIDFactory(batch=batch, uuid="96df0dff-43ec-4899-a7d9-e9d63ef12b19")
    user = UserFactory(is_superuser=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    with mock.patch(
        "xero_app.models.get_xero_invoice_data"
    ) as xero_invoice_data:
        xero_invoice_data.return_value = {
            "OnlineInvoices": [
                {"OnlineInvoiceUrl": "http://test.xero.invoice.url/"}
            ]
        }
        response = client.get(reverse("xero.batch.detail", args=[batch.pk]))
    assert HTTPStatus.OK == response.status_code
    assert "xero_batch_url" in response.context
    assert "xero_batch_uuid" in response.context
    assert "xero_payment_uuid" in response.context
    assert "http://test.xero.invoice.url/" == response.context["xero_batch_url"]
    assert (
        "96df0dff-43ec-4899-a7d9-e9d63ef12b19"
        == response.context["xero_batch_uuid"]
    )
    assert (
        "059b59ec-baf0-4e2b-99a7-c6ad83addd92"
        == response.context["xero_payment_uuid"]
    )


@pytest.mark.django_db
def test_xero_currency_update(client):
    currency = CurrencyFactory(slug="XYZ")
    xero_currency = XeroCurrencyFactory(
        code="A", description="Apple", currency=CurrencyFactory()
    )
    # login
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("xero.currency.update", args=[xero_currency.pk])
    response = client.post(url, data={"currency": currency.pk})
    # check
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("xero.currency.list") == response.url
    xero_currency.refresh_from_db()
    assert currency == xero_currency.currency


@pytest.mark.django_db
def test_xero_currency_update_missing_currency(client):
    xero_currency = XeroCurrencyFactory(
        code="A", description="Apple", currency=None
    )
    # login
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("xero.currency.update", args=[xero_currency.pk])
    response = client.post(url)
    # check
    assert HTTPStatus.OK == response.status_code
    form = response.context["form"]
    assert {
        "__all__": ["Please select a currency and VAT codes."]
    } == form.errors


@pytest.mark.django_db
def test_xero_invoice_sync(client):
    invoice = InvoiceFactory()
    user = UserFactory(is_superuser=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("xero.invoice.sync", args=[invoice.pk])
    response = client.post(url)
    # check
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("xero.invoice.detail", args=[invoice.pk]) == response.url


@pytest.mark.django_db
def test_xero_invoice_sync_payment(client):
    invoice = InvoiceFactory()
    user = UserFactory(is_superuser=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("xero.invoice.sync.payment", args=[invoice.pk])
    response = client.post(url)
    # check
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("xero.invoice.detail", args=[invoice.pk]) == response.url


@pytest.mark.django_db
def test_xero_settings_update(client):
    account = AccountFactory()
    currency = CurrencyFactory()
    # login
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("xero.settings.update")
    response = client.post(
        url,
        data={
            "days_to_sync": 3,
            "invoice_account": account.pk,
            "base_currency": currency.pk,
        },
    )
    # check
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("project.settings") == response.url
    assert 1 == XeroSettings.objects.count()
    xero_settings = XeroSettings.load()
    assert 3 == xero_settings.days_to_sync
    assert account == xero_settings.invoice_account


@pytest.mark.django_db
def test_xero_settings_update_days_to_sync_is_zero(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("xero.settings.update")
    response = client.post(url, data={"days_to_sync": 0})
    # check
    assert HTTPStatus.OK == response.status_code, response.context[
        "form"
    ].errors
    form = response.context["form"]
    assert {
        "__all__": [
            "Please select the default account for posting individual invoices"
        ],
        "days_to_sync": ["Days to sync must be greater than 1"],
    } == form.errors


@pytest.mark.django_db
def test_xero_settings_update_missing_base_currency(client):
    account = AccountFactory()
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("xero.settings.update")
    response = client.post(
        url, data={"days_to_sync": 5, "invoice_account": account.pk}
    )
    # check
    assert HTTPStatus.OK == response.status_code, response.context[
        "form"
    ].errors
    form = response.context["form"]
    assert {"__all__": ["You must select a base currency"]} == form.errors


@pytest.mark.django_db
def test_xero_settings_update_missing_invoice_account(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("xero.settings.update")
    response = client.post(url, data={"days_to_sync": 5})
    # check
    assert HTTPStatus.OK == response.status_code, response.context[
        "form"
    ].errors
    form = response.context["form"]
    assert {
        "__all__": [
            "Please select the default account for posting individual invoices"
        ]
    } == form.errors


@pytest.mark.django_db
def test_xero_settings_update_missing_days_to_sync(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.post(reverse("xero.settings.update"))
    # check
    assert HTTPStatus.OK == response.status_code, response.context[
        "form"
    ].errors
    form = response.context["form"]
    assert {
        "__all__": [
            "Please select the default account for posting individual invoices"
        ],
        "days_to_sync": ["This field is required."],
    } == form.errors


@pytest.mark.django_db
def test_xero_vat_code_update(client):
    vat_code = VatCode.objects.get(slug=VatCode.ZERO_RATE_EC)
    xero_vat_code = XeroVatCodeFactory(vat_code=None)
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.post(
        reverse("xero.vat.code.update", args=[xero_vat_code.pk]),
        data={"vat_code": vat_code.pk},
    )
    # check
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    xero_vat_code.refresh_from_db()
    assert vat_code == xero_vat_code.vat_code


@pytest.mark.django_db
def test_xero_vat_code_update_already_exists(client):
    vat_code_standard = VatCode.objects.get(slug=VatCode.STANDARD)
    vat_code_zero_rate_ec = VatCode.objects.get(slug=VatCode.ZERO_RATE_EC)
    XeroVatCodeFactory(code="INPUT2", vat_code=vat_code_standard)
    xero_vat_code = XeroVatCodeFactory(vat_code=vat_code_zero_rate_ec)
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.post(
        reverse("xero.vat.code.update", args=[xero_vat_code.pk]),
        data={"vat_code": vat_code_standard.pk},
    )
    # check
    assert HTTPStatus.OK == response.status_code
    assert {
        "__all__": ["VAT Code 'S' is already linked to Xero VAT code 'INPUT2'"]
    } == response.context["form"].errors


@pytest.mark.django_db
def test_xero_vat_code_update_existing(client):
    vat_code = VatCode.objects.get(slug=VatCode.STANDARD)
    xero_vat_code = XeroVatCodeFactory(code="INPUT2", vat_code=vat_code)
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.post(
        reverse("xero.vat.code.update", args=[xero_vat_code.pk]),
        data={"vat_code": vat_code.pk},
    )
    # check
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
