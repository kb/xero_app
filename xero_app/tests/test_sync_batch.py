# -*- encoding: utf-8 -*-
import uuid
import pytest

from datetime import date
from decimal import Decimal
from django.utils import timezone
from freezegun import freeze_time
from unittest import mock

from contact.tests.factories import ContactFactory
from finance.models import Currency, VatCode
from finance.tests.factories import CurrencyFactory
from invoice.models import Batch
from invoice.tests.factories import (
    BatchFactory,
    BatchInvoiceFactory,
    InvoiceFactory,
    InvoiceLineFactory,
    PaymentProcessorFactory,
)
from xero_app.models import (
    BatchPaymentUUID,
    BatchUUID,
    ContactUUID,
    SyncBatch,
    XeroError,
)
from .factories import (
    AccountFactory,
    BatchAccountFactory,
    BatchUUIDFactory,
    ContactUUIDFactory,
    XeroCurrencyFactory,
    XeroSettingsFactory,
    XeroVatCodeFactory,
)


@pytest.mark.django_db
def test_batch_payment_to_xero_data():
    currency = CurrencyFactory()
    payment_processor = PaymentProcessorFactory()
    batch = BatchFactory(
        batch_date=date(2019, 9, 27),
        currency=currency,
        payment_processor=payment_processor,
    )
    BatchUUIDFactory(batch=batch, uuid="96df0dff-43ec-4899-a7d9-e9d63ef12b19")
    invoice = InvoiceFactory(
        contact=ContactFactory(),
        invoice_date=date(2019, 9, 8),
        currency=currency,
        upfront_payment=True,
        upfront_payment_processor=payment_processor,
    )
    InvoiceLineFactory(
        invoice=invoice,
        description="Orange",
        quantity=Decimal("2"),
        price=Decimal("30.50"),
    ).save_and_calculate()
    BatchInvoiceFactory(batch=batch, invoice=invoice)
    BatchAccountFactory(
        currency=currency,
        payment_processor=payment_processor,
        payment_account=AccountFactory(code="0333", field_name="AccountID"),
    )
    sync_batch = SyncBatch()
    assert {
        "Invoice": {"InvoiceID": "96df0dff-43ec-4899-a7d9-e9d63ef12b19"},
        "Account": {"AccountID": "0333"},
        "Date": date(2019, 9, 27),
        "Amount": "73.20",
    } == sync_batch._batch_payment_to_xero_data(batch)


@pytest.mark.django_db
def test_batch_to_xero_data():
    currency = Currency.objects.get(slug=Currency.EURO)
    XeroCurrencyFactory(currency=currency, code="EU")
    XeroSettingsFactory(
        base_currency=Currency.objects.get(slug=Currency.POUND),
        # vat_code_standard=XeroVatCodeFactory(code="INPUT2"),
        # vat_code_zero_rated=XeroVatCodeFactory(code="ECZROUTPUT"),
    )
    XeroVatCodeFactory(
        code="INPUT2", vat_code=VatCode.objects.get(slug=VatCode.STANDARD)
    )
    payment_processor = PaymentProcessorFactory()
    batch = BatchFactory(
        batch_date=date(2019, 9, 27),
        currency=currency,
        exchange_rate=Decimal("0.88"),
        payment_processor=payment_processor,
    )
    invoice = InvoiceFactory(
        contact=ContactFactory(),
        invoice_date=date(2019, 9, 8),
        currency=currency,
        upfront_payment=True,
        upfront_payment_processor=payment_processor,
    )
    InvoiceLineFactory(
        invoice=invoice,
        description="Orange",
        quantity=Decimal("2"),
        price=Decimal("30.50"),
    ).save_and_calculate()
    BatchInvoiceFactory(batch=batch, invoice=invoice)
    batch_account = BatchAccountFactory(
        contact=ContactFactory(),
        currency=currency,
        invoice_account=AccountFactory(code="0222", field_name="Code"),
        payment_account=AccountFactory(code="0333", field_name="AccountID"),
        payment_processor=payment_processor,
    )
    ContactUUIDFactory(
        contact=batch_account.contact,
        uuid="96df0dff-43ec-4899-a7d9-e9d63ef12b19",
    )
    # test
    sync_batch = SyncBatch()
    with freeze_time("2017-05-21 23:55:01"):
        result = sync_batch._batch_to_xero_data(batch)
    # print(json.dumps(result, indent=4, cls=DjangoJSONEncoder))
    assert {
        "AmountCredited": 0.0,
        "AmountDue": Decimal("73.20"),
        "AmountPaid": 0.0,
        "Contact": {"ContactID": "96df0dff-43ec-4899-a7d9-e9d63ef12b19"},
        "CreditNotes": [],
        "CurrencyCode": "EU",
        "CurrencyRate": "1.1364",
        "Date": date(2019, 9, 27),
        "DueDate": date(2019, 9, 27),
        "HasAttachments": False,
        "HasErrors": False,
        "InvoiceNumber": "BATCH{:06d}".format(batch.pk),
        "IsDiscounted": False,
        "LineAmountTypes": "Exclusive",
        "LineItems": [
            {
                "AccountCode": "0222",
                "Description": "Summary for VAT code 'S' (1 items)",
                "Quantity": "1.00",
                "TaxAmount": "12.20",
                "TaxType": "INPUT2",
                "UnitAmount": "61.00",
            }
        ],
        "Overpayments": [],
        "Payments": [],
        "Prepayments": [],
        "Reference": "",
        "Status": "AUTHORISED",
        "SubTotal": "61.00",
        "Total": "73.20",
        "TotalTax": "12.20",
        "Type": "ACCREC",
        "UpdatedDateUTC": "2017-05-21T23:55:01+00:00",
        "Url": "http://localhost/batch/{}/invoice/".format(batch.pk),
    } == result


@pytest.mark.django_db
def test_batch_to_xero_data_contact_not_synced():
    currency = Currency.objects.get(slug=Currency.EURO)
    payment_processor = PaymentProcessorFactory()
    batch = BatchFactory(currency=currency, payment_processor=payment_processor)
    batch_account = BatchAccountFactory(
        contact=ContactFactory(company_name="KB Software Ltd"),
        currency=currency,
        payment_processor=payment_processor,
    )
    sync_batch = SyncBatch()
    with pytest.raises(XeroError) as e:
        sync_batch._batch_to_xero_data(batch)
    assert (
        "Contact KB Software Ltd (primary key {}) has not "
        "been created on Xero".format(batch_account.contact.pk)
    ) in str(e.value)


@pytest.mark.django_db
def test_sync():
    """Sync a batch."""
    batch_date = date(2018, 2, 22)
    contact = ContactFactory()
    currency = Currency.objects.get(slug=Currency.POUND)
    exchange_rate = Decimal("1")
    invoice_account = AccountFactory()
    payment_processor = PaymentProcessorFactory()
    vat_code_standard = VatCode.objects.get(slug=VatCode.STANDARD)
    XeroCurrencyFactory(currency=currency)
    XeroVatCodeFactory(vat_code=VatCode.objects.get(slug="S"))
    invoice = InvoiceFactory(
        invoice_date=batch_date,
        currency=currency,
        exchange_rate=exchange_rate,
        upfront_payment=True,
        upfront_payment_processor=payment_processor,
    )
    InvoiceLineFactory(
        invoice=invoice,
        net=Decimal(100),
        vat=Decimal(20),
        vat_code=vat_code_standard,
    )
    batch = Batch.objects.create_batch(
        batch_date, currency, exchange_rate, payment_processor
    )
    BatchAccountFactory(
        contact=contact,
        currency=currency,
        payment_processor=payment_processor,
        invoice_account=invoice_account,
    )
    assert BatchUUID.objects.get_if_exists(batch) is None
    assert ContactUUID.objects.get_if_exists(contact) is None
    assert BatchPaymentUUID.objects.get_if_exists(batch) is None
    sync_batch = SyncBatch()
    with mock.patch("xero_app.models.connect") as xero_connect:
        xero_connect.return_value.contacts.put.return_value = [
            {"ContactID": str(uuid.uuid4())}
        ]
        xero_connect.return_value.invoices.put.return_value = [
            {"InvoiceID": str(uuid.uuid4())}
        ]
        xero_connect.return_value.payments.put.return_value = [
            {"PaymentID": str(uuid.uuid4())}
        ]
        sync_batch.sync(batch)
    assert BatchUUID.objects.get_if_exists(batch) is not None
    assert ContactUUID.objects.get_if_exists(contact) is not None
    assert BatchPaymentUUID.objects.get_if_exists(batch) is not None


@pytest.mark.django_db
def test_sync_batch_total_zero():
    """Sync a batch where the total is zero.

    https://www.kbsoftware.co.uk/crm/ticket/4510/

    .. tip:: See ``test_sync`` for a standard sync...

    """
    batch_date = date(2018, 2, 22)
    contact = ContactFactory()
    currency = Currency.objects.get(slug=Currency.POUND)
    exchange_rate = Decimal("1")
    invoice_account = AccountFactory()
    payment_processor = PaymentProcessorFactory()
    VatCode.objects.get(slug=VatCode.STANDARD)
    XeroCurrencyFactory(currency=currency)
    XeroVatCodeFactory(vat_code=VatCode.objects.get(slug="S"))
    InvoiceFactory(
        invoice_date=batch_date,
        currency=currency,
        exchange_rate=exchange_rate,
        upfront_payment=True,
        upfront_payment_processor=payment_processor,
    )
    batch = Batch.objects.create_batch(
        batch_date, currency, exchange_rate, payment_processor
    )
    BatchAccountFactory(
        contact=contact,
        currency=currency,
        payment_processor=payment_processor,
        invoice_account=invoice_account,
    )
    net, vat = batch.net_and_vat()
    assert Decimal() == net
    assert Decimal() == vat
    assert BatchUUID.objects.get_if_exists(batch) is None
    assert ContactUUID.objects.get_if_exists(contact) is None
    assert BatchPaymentUUID.objects.get_if_exists(batch) is None
    sync_batch = SyncBatch()
    with mock.patch("xero_app.models.connect") as xero_connect:
        xero_connect.return_value.contacts.put.return_value = [
            {"ContactID": str(uuid.uuid4())}
        ]
        xero_connect.return_value.invoices.put.return_value = [
            {"InvoiceID": str(uuid.uuid4())}
        ]
        xero_connect.return_value.payments.put.return_value = [
            {"PaymentID": str(uuid.uuid4())}
        ]
        sync_batch.sync(batch)
    assert BatchUUID.objects.get_if_exists(batch) is not None
    assert ContactUUID.objects.get_if_exists(contact) is not None
    # no payment for batches where net and vat are zero
    assert BatchPaymentUUID.objects.get_if_exists(batch) is None


@pytest.mark.django_db
def test_xero_currency_for_batch():
    currency = CurrencyFactory()
    xero_currency = XeroCurrencyFactory(currency=currency)
    batch = BatchFactory(currency=currency)
    sync_batch = SyncBatch()
    assert xero_currency == sync_batch._xero_currency_for_batch(batch)


@pytest.mark.django_db
def test_xero_currency_for_batch_no_currency():
    currency = Currency.objects.get(slug="EUR")
    batch = BatchFactory(currency=currency)
    sync_batch = SyncBatch()
    with pytest.raises(XeroError) as e:
        sync_batch._xero_currency_for_batch(batch)
    assert (
        "Cannot find the Xero currency for batch {} ('EUR')".format(batch.pk)
    ) in str(e.value)


@pytest.mark.django_db
def test_xero_invoice_account_for_batch():
    currency = Currency.objects.get(slug=Currency.EURO)
    payment_processor = PaymentProcessorFactory()
    invoice_account = AccountFactory()
    batch = BatchFactory(
        batch_date=timezone.now(),
        currency=currency,
        payment_processor=payment_processor,
    )
    BatchAccountFactory(
        currency=currency,
        payment_processor=payment_processor,
        invoice_account=invoice_account,
    )
    sync_batch = SyncBatch()
    assert invoice_account == sync_batch._xero_invoice_account_for_batch(batch)


@pytest.mark.django_db
def test_xero_invoice_account_for_batch_no_invoice_account():
    currency = Currency.objects.get(slug=Currency.EURO)
    payment_processor = PaymentProcessorFactory()
    with freeze_time("2017-05-21 23:55:01"):
        batch = BatchFactory(
            batch_date=timezone.now(),
            currency=currency,
            payment_processor=payment_processor,
        )
    BatchAccountFactory(
        currency=currency,
        payment_processor=payment_processor,
        invoice_account=None,
    )
    sync_batch = SyncBatch()
    with pytest.raises(XeroError) as e:
        sync_batch._xero_invoice_account_for_batch(batch)
    assert (
        "Cannot find a Xero invoice account for "
        "batch {} (21/05/2017)".format(batch.pk)
    ) in str(e.value)


@pytest.mark.django_db
def test_xero_payment_account_for_batch():
    account = AccountFactory()
    currency = CurrencyFactory()
    payment_processor = PaymentProcessorFactory()
    batch = BatchFactory(currency=currency, payment_processor=payment_processor)
    BatchAccountFactory(
        currency=currency,
        payment_processor=payment_processor,
        payment_account=account,
    )
    sync_batch = SyncBatch()
    assert account == sync_batch._xero_payment_account_for_batch(batch)


@pytest.mark.django_db
def test_xero_payment_account_for_batch_no_batch_account():
    batch = BatchFactory(batch_date=date(2019, 1, 31))
    sync_batch = SyncBatch()
    with pytest.raises(XeroError) as e:
        sync_batch._xero_payment_account_for_batch(batch)
    assert (
        "Cannot find a Xero payment account for batch {} (31/01/2019)".format(
            batch.pk
        )
    ) in str(e.value)
