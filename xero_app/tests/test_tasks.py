# -*- encoding: utf-8 -*-
import os
import uuid
import pytest
import responses

from decimal import Decimal
from http import HTTPStatus
from unittest import mock

from invoice.tests.factories import (
    BatchFactory,
    BatchInvoiceFactory,
    InvoiceCreditFactory,
    InvoiceFactory,
    PaymentProcessorFactory,
)
from xero_app.tasks import xero_sync_invoice, xero_sync_payment
from xero_app.tests.factories import (
    AccountFactory,
    BatchAccountFactory,
    InvoiceUUIDFactory,
    XeroCurrencyFactory,
    XeroSettingsFactory,
)


@pytest.mark.django_db
@responses.activate
def test_xero_sync_invoice(settings):
    settings.XERO_KEY_FILE = os.path.join(
        os.path.dirname(os.path.realpath(__file__)), "data", "privatekey.pem"
    )
    invoice = InvoiceFactory(exchange_rate=Decimal("1"))
    XeroCurrencyFactory(currency=invoice.currency)
    XeroSettingsFactory(invoice_account=AccountFactory(code="010203"))
    contact_uuid = str(uuid.uuid4())
    invoice_uuid = str(uuid.uuid4())
    responses.add(
        responses.PUT,
        "https://api.xero.com/api.xro/2.0/Contacts",
        json={"Status": "OK", "Contacts": [{"ContactID": contact_uuid}]},
        status=HTTPStatus.OK,
    )
    responses.add(
        responses.PUT,
        "https://api.xero.com/api.xro/2.0/Invoices",
        json={"Status": "OK", "Invoices": [{"InvoiceID": invoice_uuid}]},
        status=HTTPStatus.OK,
    )
    assert invoice.pk == xero_sync_invoice(invoice.pk)


@pytest.mark.django_db
@responses.activate
def test_xero_sync_invoice_credit_note(settings):
    settings.XERO_KEY_FILE = os.path.join(
        os.path.dirname(os.path.realpath(__file__)), "data", "privatekey.pem"
    )
    payment_processor = PaymentProcessorFactory()
    credit_note = InvoiceFactory(is_credit=True, exchange_rate=Decimal("1"))
    invoice = InvoiceFactory(
        contact=credit_note.contact,
        upfront_payment=True,
        upfront_payment_processor=payment_processor,
    )
    InvoiceCreditFactory(invoice=invoice, credit_note=credit_note)
    # invoice batch
    batch = BatchFactory(
        currency=invoice.currency, payment_processor=payment_processor
    )
    BatchInvoiceFactory(batch=batch, invoice=invoice)
    # xero batch
    BatchAccountFactory(
        currency=invoice.currency, payment_processor=payment_processor
    )
    XeroCurrencyFactory(currency=credit_note.currency)
    XeroSettingsFactory(invoice_account=AccountFactory(code="010203"))
    contact_uuid = str(uuid.uuid4())
    credit_note_uuid = str(uuid.uuid4())
    responses.add(
        responses.PUT,
        "https://api.xero.com/api.xro/2.0/Contacts",
        json={"Status": "OK", "Contacts": [{"ContactID": contact_uuid}]},
        status=HTTPStatus.OK,
    )
    responses.add(
        responses.PUT,
        "https://api.xero.com/api.xro/2.0/CreditNotes",
        json={
            "Status": "OK",
            "CreditNotes": [{"CreditNoteID": credit_note_uuid}],
        },
        status=HTTPStatus.OK,
    )
    assert credit_note.pk == xero_sync_invoice(credit_note.pk)


@mock.patch("requests.post")
@pytest.mark.django_db
def test_xero_sync_payment(mock_requests_post):
    invoice = InvoiceFactory()
    with pytest.raises(NotImplementedError):
        xero_sync_payment(invoice.pk)


@mock.patch("requests.post")
@pytest.mark.django_db
def test_xero_sync_payment_credit_note(mock_requests_post, settings):
    settings.XERO_KEY_FILE = os.path.join(
        os.path.dirname(os.path.realpath(__file__)), "data", "privatekey.pem"
    )
    credit_note = InvoiceFactory(is_credit=True, exchange_rate=Decimal("1"))
    InvoiceUUIDFactory(invoice=credit_note)
    payment_processor = PaymentProcessorFactory()
    invoice = InvoiceFactory(
        contact=credit_note.contact,
        upfront_payment=True,
        upfront_payment_processor=payment_processor,
    )
    InvoiceCreditFactory(invoice=invoice, credit_note=credit_note)
    # invoice batch
    batch = BatchFactory(
        currency=invoice.currency, payment_processor=payment_processor
    )
    BatchInvoiceFactory(batch=batch, invoice=invoice)
    # xero batch
    BatchAccountFactory(
        currency=invoice.currency, payment_processor=payment_processor
    )
    assert credit_note.pk == xero_sync_payment(credit_note.pk)
