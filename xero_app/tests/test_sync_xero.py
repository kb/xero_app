# -*- encoding: utf-8 -*-
import os
import pytest
import responses
import uuid

from django.utils import timezone
from http import HTTPStatus
from freezegun import freeze_time

from contact.tests.factories import (
    ContactAddressFactory,
    ContactEmailFactory,
    ContactFactory,
    ContactPhoneFactory,
)
from finance.models import Currency
from finance.tests.factories import CurrencyFactory
from invoice.tests.factories import InvoiceFactory
from login.tests.factories import UserFactory
from xero_app.tests.factories import ContactUUIDFactory
from xero_app.models import _xero_time, ContactUUID, SyncXero, XeroError
from .factories import XeroCurrencyFactory


def test_api_result():
    sync_xero = SyncXero()
    xero_response = [{"ContactID": "my-uuid-23"}]
    assert "my-uuid-23" == sync_xero._api_result(xero_response, "ContactID")


def test_api_result_cannot_find():
    sync_xero = SyncXero()
    xero_response = [{"AppleID": "my-uuid-23"}]
    with pytest.raises(XeroError) as e:
        sync_xero._api_result(xero_response, "ContactID")
    assert (
        "Cannot find uuid in result using 'ContactID': "
        "{'AppleID': 'my-uuid-23'}"
    ) in str(e.value)


def test_api_result_check_uuid():
    sync_xero = SyncXero()
    xero_response = [{"ContactID": "my-uuid-23"}]
    assert "my-uuid-23" == sync_xero._api_result(
        xero_response, "ContactID", "my-uuid-23"
    )


def test_api_result_check_uuid_no_match():
    sync_xero = SyncXero()
    xero_response = [{"ContactID": "my-uuid-23"}]
    with pytest.raises(XeroError) as e:
        sync_xero._api_result(xero_response, "ContactID", "my-uuid-44")
    assert (
        "Original uuid 'my-uuid-44' does not match Xero uuid: 'my-uuid-23'"
        in str(e.value)
    )


def test_api_result_no_data():
    xero_response = {}
    sync_xero = SyncXero()
    with pytest.raises(XeroError) as e:
        sync_xero._api_result(xero_response, "ContactID")
    assert "No data (or too much data) returned from Xero" in str(e.value)


@pytest.mark.django_db
def test_contact_to_xero_data_create():
    user = UserFactory(first_name="Patrick", last_name="Kimber")
    contact = ContactFactory(user=user, company_name="KB Software Ltd")
    ContactEmailFactory(contact=contact, email="patrick@kbsoftware.co.uk")
    mobile = ContactPhoneFactory(contact=contact, phone="07840 333 444")
    mobile.tags.add("Mobile")
    phone = ContactPhoneFactory(contact=contact, phone="01837 111 222")
    phone.tags.add("Landline")
    address = ContactAddressFactory(
        contact=contact,
        address_one="2 Honeydown",
        address_two="Strawbridge",
        locality="Hatherleigh",
        town="Okehampton",
        admin_area="Devon",
        postal_code="EX20 1FR",
        country="UK",
    )
    address.tags.add("Business")
    # test
    sync_xero = SyncXero()
    with freeze_time("2017-05-21 23:55:01"):
        result = sync_xero._contact_to_xero_data(contact)
    # import json
    # from django.core.serializers.json import DjangoJSONEncoder
    # print(json.dumps(result, indent=4, cls=DjangoJSONEncoder))
    assert {
        "ContactNumber": contact.pk,
        "ContactStatus": "ACTIVE",
        "Name": "KB Software Ltd",
        "FirstName": "Patrick",
        "LastName": "Kimber",
        "EmailAddress": "patrick@kbsoftware.co.uk",
        "BankAccountDetails": "",
        "Addresses": [
            {
                "AddressType": "STREET",
                "AddressLine1": "2 Honeydown",
                "AddressLine2": "Strawbridge",
                "AddressLine3": "Hatherleigh",
                "City": "Okehampton",
                "Region": "Devon",
                "PostalCode": "EX20 1FR",
                "Country": "UK",
                "AttentionTo": "",
            }
        ],
        "Phones": [
            {
                "PhoneType": "DEFAULT",
                "PhoneNumber": "01837 111 222",
                "PhoneAreaCode": "",
                "PhoneCountryCode": "",
            },
            {
                "PhoneType": "MOBILE",
                "PhoneNumber": "07840 333 444",
                "PhoneAreaCode": "",
                "PhoneCountryCode": "",
            },
        ],
        "UpdatedDateUTC": "2017-05-21T23:55:01+00:00",
        "ContactGroups": [],
        "IsSupplier": False,
        "IsCustomer": True,
        "ContactPersons": [],
        "HasAttachments": False,
        "HasValidationErrors": False,
        "Url": "http://localhost/contact/{}/".format(contact.pk),
    } == result


@pytest.mark.django_db
def test_contact_to_xero_data_update():
    user = UserFactory(first_name="Patrick", last_name="Kimber")
    contact = ContactFactory(user=user, company_name="KB Software Ltd")
    ContactEmailFactory(contact=contact, email="patrick@kbsoftware.co.uk")
    # get the uuid
    ContactUUID.objects.init_contact(contact)
    x = str(uuid.uuid4())
    ContactUUID.objects.update_uuid(contact, x)
    contact_uuid = ContactUUID.objects.get_if_exists(contact)
    assert contact_uuid is not None
    # test
    sync_xero = SyncXero()
    with freeze_time("2017-05-21 23:55:01"):
        result = sync_xero._contact_to_xero_data(
            contact, contact_uuid.uuid_as_str()
        )
    assert {
        "ContactID": x,
        "ContactNumber": contact.pk,
        "ContactStatus": "ACTIVE",
        "Name": "KB Software Ltd",
        "FirstName": "Patrick",
        "LastName": "Kimber",
        "EmailAddress": "patrick@kbsoftware.co.uk",
        "BankAccountDetails": "",
        "Addresses": [],
        "Phones": [],
        "UpdatedDateUTC": "2017-05-21T23:55:01+00:00",
        "ContactGroups": [],
        "IsSupplier": False,
        "IsCustomer": True,
        "ContactPersons": [],
        "HasAttachments": False,
        "HasValidationErrors": False,
        "Url": "http://localhost/contact/{}/".format(contact.pk),
    } == result


@pytest.mark.django_db
@responses.activate
def test_sync_contact_to_xero(settings):
    settings.XERO_KEY_FILE = os.path.join(
        os.path.dirname(os.path.realpath(__file__)), "data", "privatekey.pem"
    )
    contact_uuid = str(uuid.uuid4())
    responses.add(
        responses.PUT,
        "https://api.xero.com/api.xro/2.0/Contacts",
        json={"Status": "OK", "Contacts": [{"ContactID": contact_uuid}]},
        status=HTTPStatus.OK,
    )
    contact = ContactFactory()
    assert 0 == ContactUUID.objects.count()
    sync_xero = SyncXero()
    sync_xero._sync_contact_to_xero(contact)
    assert 1 == ContactUUID.objects.count()
    contact_uuid = ContactUUID.objects.first()
    assert contact == contact_uuid.contact
    assert contact_uuid.uuid == contact_uuid.uuid


@pytest.mark.django_db
@responses.activate
def test_sync_contact_to_xero_already_exists_synced(settings):
    settings.XERO_KEY_FILE = os.path.join(
        os.path.dirname(os.path.realpath(__file__)), "data", "privatekey.pem"
    )
    contact_uuid = str(uuid.uuid4())
    responses.add(
        responses.POST,
        "https://api.xero.com/api.xro/2.0/Contacts",
        json={"Status": "OK", "Contacts": [{"ContactID": contact_uuid}]},
        status=HTTPStatus.OK,
    )
    contact = ContactFactory()
    ContactUUIDFactory(contact=contact, uuid=contact_uuid)
    sync_xero = SyncXero()
    sync_xero._sync_contact_to_xero(contact)
    assert 1 == ContactUUID.objects.count()


@pytest.mark.django_db
@responses.activate
def test_sync_contact_to_xero_already_exists_not_synced(settings):
    settings.XERO_KEY_FILE = os.path.join(
        os.path.dirname(os.path.realpath(__file__)), "data", "privatekey.pem"
    )
    contact = ContactFactory(company_name="kb")
    contact_uuid = ContactUUIDFactory(contact=contact, uuid=None)
    sync_xero = SyncXero()
    with pytest.raises(XeroError) as e:
        sync_xero._sync_contact_to_xero(contact)
    assert (
        "ContactUUID ({}) kb (primary key {}) (not synced) "
        "failed sync to Xero previously".format(contact_uuid.pk, contact.pk)
    ) in str(e.value)


@pytest.mark.django_db
def test_xero_currency_for_invoice():
    currency = CurrencyFactory()
    xero_currency = XeroCurrencyFactory(currency=currency)
    invoice = InvoiceFactory(currency=currency)
    sync_xero = SyncXero()
    assert xero_currency == sync_xero._xero_currency_for_invoice(invoice)


@pytest.mark.django_db
def test_xero_currency_for_invoice_no_currency():
    currency = Currency.objects.get(slug="EUR")
    invoice = InvoiceFactory(currency=currency)
    sync_xero = SyncXero()
    with pytest.raises(XeroError) as e:
        sync_xero._xero_currency_for_invoice(invoice)
    assert (
        "Cannot find the Xero currency for invoice {} "
        "('EUR')".format(invoice.pk)
    ) in str(e.value)


def test_xero_time():
    with freeze_time("2017-05-21 23:55:01"):
        assert "2017-05-21T23:55:01+00:00" == _xero_time(timezone.now())
