# -*- encoding: utf-8 -*-
import pytest
import uuid

from datetime import date

from invoice.tests.factories import BatchFactory
from xero_app.models import BatchUUID, XeroError


@pytest.mark.django_db
def test_batch_uuid():
    batch = BatchFactory()
    assert BatchUUID.objects.get_if_exists(batch) is None
    x = str(uuid.uuid4())
    # init
    BatchUUID.objects.init_batch(batch)
    # get_if_exists (should fail before '_update_uuid')
    with pytest.raises(XeroError) as e:
        BatchUUID.objects.get_if_exists(batch)
    assert "failed sync to Xero previously" in str(e.value)
    assert 1 == BatchUUID.objects.count()
    batch_uuid = BatchUUID.objects.first()
    # is_synced
    assert batch_uuid.is_synced is False
    assert 1 == BatchUUID.objects.count()
    assert batch == batch_uuid.batch
    # update_uuid
    BatchUUID.objects.update_uuid(batch, x)
    assert 1 == BatchUUID.objects.count()
    # get_if_exists
    batch_uuid = BatchUUID.objects.get_if_exists(batch)
    assert batch_uuid is not None
    # is_synced
    assert batch_uuid.is_synced is True
    assert x == str(batch_uuid.uuid)
    # update_uuid (again)
    with pytest.raises(XeroError) as e:
        BatchUUID.objects.update_uuid(batch, x)
    assert "has already been synced" in str(e.value)


@pytest.mark.django_db
def test_str():
    batch = BatchFactory(batch_date=date(2017, 4, 23))
    # init_invoice
    BatchUUID.objects.init_batch(batch)
    assert 1 == BatchUUID.objects.count()
    batch_uuid = BatchUUID.objects.first()
    assert "Batch {} for 23/04/2017 (not synced)".format(batch.pk) == str(
        batch_uuid
    )
    # update_uuid
    x = str(uuid.uuid4())
    BatchUUID.objects.update_uuid(batch, x)
    assert 1 == BatchUUID.objects.count()
    batch_uuid = BatchUUID.objects.first()
    assert "Batch {} for 23/04/2017: {}".format(batch.pk, x) == str(batch_uuid)
