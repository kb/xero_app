# -*- encoding: utf-8 -*-
import pytest
import uuid

from contact.tests.factories import ContactFactory
from invoice.tests.factories import InvoiceFactory
from login.tests.factories import UserFactory
from xero_app.models import PaymentUUID, XeroError


@pytest.mark.django_db
def test_payment_uuid():
    invoice = InvoiceFactory()
    assert PaymentUUID.objects.get_if_exists(invoice) is None
    x = str(uuid.uuid4())
    # init
    PaymentUUID.objects.init_invoice(invoice)
    # get_if_exists (should fail before '_update_uuid')
    with pytest.raises(XeroError) as e:
        PaymentUUID.objects.get_if_exists(invoice)
    assert "failed sync to Xero previously" in str(e.value)
    assert 1 == PaymentUUID.objects.count()
    payment_uuid = PaymentUUID.objects.first()
    # is_synced
    assert payment_uuid.is_synced is False
    assert 1 == PaymentUUID.objects.count()
    assert invoice == payment_uuid.invoice
    # update_uuid
    PaymentUUID.objects.update_uuid(invoice, x)
    assert 1 == PaymentUUID.objects.count()
    # get_if_exists
    payment_uuid = PaymentUUID.objects.get_if_exists(invoice)
    assert payment_uuid is not None
    # is_synced
    assert payment_uuid.is_synced is True
    assert x == str(payment_uuid.uuid)
    # update_uuid (again)
    with pytest.raises(XeroError) as e:
        PaymentUUID.objects.update_uuid(invoice, x)
    assert "has already been synced" in str(e.value)


@pytest.mark.django_db
def test_str():
    contact = ContactFactory(
        company_name="", user=UserFactory(first_name="P", last_name="Kimber")
    )
    invoice = InvoiceFactory(contact=contact, number=222)
    # init_invoice
    PaymentUUID.objects.init_invoice(invoice)
    assert 1 == PaymentUUID.objects.count()
    payment_uuid = PaymentUUID.objects.first()
    assert (
        "Payment for invoice 000222 (primary key {}) "
        "for P Kimber (not synced)".format(invoice.pk)
    ) == str(payment_uuid)
    # update_uuid
    x = str(uuid.uuid4())
    PaymentUUID.objects.update_uuid(invoice, x)
    assert 1 == PaymentUUID.objects.count()
    payment_uuid = PaymentUUID.objects.first()
    assert (
        "Payment for invoice 000222 (primary key {}) "
        "for P Kimber: {}".format(invoice.pk, x)
    ) == str(payment_uuid)
