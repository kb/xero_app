# -*- encoding: utf-8 -*-
import os
import pytest

from datetime import date
from decimal import Decimal
from freezegun import freeze_time
from unittest import mock

from xero_app.models import (
    connect,
    dates_to_sync,
    get_xero_exchange_rate,
    XeroError,
)
from .factories import XeroSettingsFactory


@mock.patch("requests.post")
def test_connect(mock_requests_post, settings):
    settings.XERO_KEY_FILE = os.path.join(
        os.path.dirname(os.path.realpath(__file__)), "data", "privatekey.pem"
    )
    connect()


@pytest.mark.django_db
def test_dates_to_sync():
    XeroSettingsFactory(days_to_sync=5)
    with freeze_time("2017-05-21"):
        assert [
            date(2017, 5, 16),
            date(2017, 5, 17),
            date(2017, 5, 18),
            date(2017, 5, 19),
            date(2017, 5, 20),
        ] == dates_to_sync()


@pytest.mark.django_db
def test_dates_to_sync_no_settings():
    with pytest.raises(XeroError) as e:
        dates_to_sync()
    assert "Xero settings have not been updated" in str(e.value)


def test_get_xero_exchange_rate():
    assert "1.1628" == get_xero_exchange_rate(Decimal("0.86"))
