# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from unittest import mock

from invoice.tests.factories import BatchFactory, InvoiceFactory
from login.tests.fixture import perm_check
from .factories import (
    BatchAccountFactory,
    XeroCurrencyFactory,
    XeroVatCodeFactory,
)


@mock.patch("requests.post")
@pytest.mark.django_db
def test_xero_batch_detail(perm_check):
    batch = BatchFactory()
    url = reverse("xero.batch.detail", args=[batch.pk])
    perm_check.superuser(url)


@pytest.mark.django_db
def test_xero_batch_sync(perm_check):
    batch = BatchFactory()
    url = reverse("xero.batch.sync", args=[batch.pk])
    perm_check.superuser(url)


@pytest.mark.django_db
def test_xero_batch_sync_update(perm_check):
    batch = BatchFactory()
    url = reverse("xero.batch.sync.payment", args=[batch.pk])
    perm_check.superuser(url)


@pytest.mark.django_db
def test_frequency_list(perm_check):
    BatchAccountFactory()
    BatchAccountFactory()
    url = reverse("xero.account.list")
    perm_check.staff(url)


@pytest.mark.django_db
def test_frequency_update(perm_check):
    batch_nominal = BatchAccountFactory()
    url = reverse("xero.account.update", args=[batch_nominal.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_currency_list(perm_check):
    XeroCurrencyFactory()
    XeroCurrencyFactory()
    url = reverse("xero.currency.list")
    perm_check.staff(url)


@pytest.mark.django_db
def test_xero_currency_update(perm_check):
    xero_currency = XeroCurrencyFactory()
    url = reverse("xero.currency.update", args=[xero_currency.pk])
    perm_check.staff(url)


@mock.patch("requests.post")
@pytest.mark.django_db
def test_xero_invoice_detail(perm_check):
    invoice = InvoiceFactory()
    url = reverse("xero.invoice.detail", args=[invoice.pk])
    perm_check.superuser(url)


@pytest.mark.django_db
def test_xero_invoice_sync(perm_check):
    invoice = InvoiceFactory()
    url = reverse("xero.invoice.sync", args=[invoice.pk])
    perm_check.superuser(url)


@pytest.mark.django_db
def test_xero_invoice_sync_update(perm_check):
    invoice = InvoiceFactory()
    url = reverse("xero.invoice.sync.payment", args=[invoice.pk])
    perm_check.superuser(url)


@pytest.mark.django_db
def test_xero_settings_update(perm_check):
    XeroCurrencyFactory()
    url = reverse("xero.settings.update")
    perm_check.staff(url)


@pytest.mark.django_db
def test_xero_vat_code_list(perm_check):
    XeroVatCodeFactory()
    XeroVatCodeFactory()
    url = reverse("xero.vat.code.list")
    perm_check.staff(url)


@pytest.mark.django_db
def test_xero_vat_code_update(perm_check):
    xero_vat_code = XeroVatCodeFactory()
    url = reverse("xero.vat.code.update", args=[xero_vat_code.pk])
    perm_check.staff(url)
