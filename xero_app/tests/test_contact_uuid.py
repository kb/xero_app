# -*- encoding: utf-8 -*-
import pytest
import uuid

from django.db import IntegrityError

from contact.tests.factories import ContactFactory
from login.tests.factories import UserFactory
from xero_app.models import ContactUUID, XeroError


@pytest.mark.django_db
def test_contact_uuid():
    contact = ContactFactory()
    assert ContactUUID.objects.get_if_exists(contact) is None
    x = str(uuid.uuid4())
    # init
    ContactUUID.objects.init_contact(contact)
    # get_if_exists (should fail before '_update_uuid')
    with pytest.raises(XeroError) as e:
        ContactUUID.objects.get_if_exists(contact)
    assert "failed sync to Xero previously" in str(e.value)
    assert 1 == ContactUUID.objects.count()
    contact_uuid = ContactUUID.objects.first()
    # is_synced
    assert contact_uuid.is_synced is False
    assert 1 == ContactUUID.objects.count()
    assert contact == contact_uuid.contact
    # update_uuid
    ContactUUID.objects.update_uuid(contact, x)
    assert 1 == ContactUUID.objects.count()
    # get_if_exists
    contact_uuid = ContactUUID.objects.get_if_exists(contact)
    assert contact_uuid is not None
    # is_synced
    assert contact_uuid.is_synced is True
    assert x == str(contact_uuid.uuid)
    # update_uuid (again)
    with pytest.raises(XeroError) as e:
        ContactUUID.objects.update_uuid(contact, x)
    assert "has already been synced" in str(e.value)


@pytest.mark.django_db
def test_init_contact_duplicate():
    contact = ContactFactory()
    ContactUUID.objects.init_contact(contact)
    with pytest.raises(IntegrityError):
        ContactUUID.objects.init_contact(contact)


@pytest.mark.django_db
def test_update_uuid_duplicate():
    contact_1 = ContactFactory()
    contact_2 = ContactFactory()
    x = str(uuid.uuid4())
    ContactUUID.objects.init_contact(contact_1)
    ContactUUID.objects.init_contact(contact_2)
    ContactUUID.objects.update_uuid(contact_1, x)
    with pytest.raises(IntegrityError):
        ContactUUID.objects.update_uuid(contact_2, x)


@pytest.mark.django_db
def test_str():
    contact = ContactFactory(
        company_name="", user=UserFactory(first_name="P", last_name="Kimber")
    )
    # init_contact
    ContactUUID.objects.init_contact(contact)
    assert 1 == ContactUUID.objects.count()
    contact_uuid = ContactUUID.objects.first()
    assert "P Kimber (primary key {}) (not synced)".format(contact.pk) == str(
        contact_uuid
    )
    # update_uuid
    x = str(uuid.uuid4())
    ContactUUID.objects.update_uuid(contact, x)
    assert 1 == ContactUUID.objects.count()
    contact_uuid = ContactUUID.objects.first()
    assert "P Kimber (primary key {}): {}".format(contact.pk, x) == str(
        contact_uuid
    )
