# -*- encoding: utf-8 -*-
import pytest

from decimal import Decimal

from finance.models import VatCode
from xero_app.models import XeroError, XeroVatCode
from .factories import XeroVatCodeFactory


@pytest.mark.django_db
def test_init_xero_vat_code():
    xero_vat_code = XeroVatCode.objects.init_xero_vat_code(
        "ECZROUTPUT", "Zero Rated EC Goods Income", 20.0, True
    )
    assert "ECZROUTPUT" == xero_vat_code.code
    assert "Zero Rated EC Goods Income" == xero_vat_code.name
    assert xero_vat_code.can_apply_to_liabilities is True
    assert Decimal("20") == xero_vat_code.rate


@pytest.mark.django_db
def test_init_xero_vat_code_existing():
    xero_vat_code = XeroVatCodeFactory(
        code="ECZROUTPUT",
        name="Zero Rated EC Goods",
        rate=15.0,
        can_apply_to_liabilities=False,
    )
    xero_vat_code = XeroVatCode.objects.init_xero_vat_code(
        "ECZROUTPUT", "Zero Rated EC Goods Income", 20.0, True
    )
    assert 1 == XeroVatCode.objects.count()
    xero_vat_code = XeroVatCode.objects.first()
    assert "ECZROUTPUT" == xero_vat_code.code
    assert "Zero Rated EC Goods Income" == xero_vat_code.name
    assert xero_vat_code.can_apply_to_liabilities is True
    assert Decimal("20") == xero_vat_code.rate


@pytest.mark.django_db
def test_ordering():
    XeroVatCodeFactory(code="c")
    XeroVatCodeFactory(code="b")
    XeroVatCodeFactory(code="a")
    assert ["a", "b", "c"] == [x.code for x in XeroVatCode.objects.all()]


@pytest.mark.django_db
def test_str():
    assert "ECZROUTPUT, Zero Rated EC Goods Income @ 15.0%" == str(
        XeroVatCodeFactory(
            code="ECZROUTPUT",
            name="Zero Rated EC Goods Income",
            rate=15.0,
            can_apply_to_liabilities=True,
        )
    )


@pytest.mark.django_db
def test_xero_vat_code_for():
    XeroVatCodeFactory(
        code="ECZROUTPUT",
        name="Zero Rated EC Goods",
        rate=Decimal(),
        can_apply_to_liabilities=False,
        vat_code=VatCode.objects.get(slug=VatCode.ZERO_RATE_EC),
    )
    assert "ECZROUTPUT" == XeroVatCode.objects.vat_code_for(
        VatCode.ZERO_RATE_EC
    )


@pytest.mark.django_db
def test_xero_vat_code_for_not_setup():
    XeroVatCodeFactory(
        code="ECZROUTPUT",
        name="Zero Rated EC Goods",
        rate=Decimal(),
        can_apply_to_liabilities=False,
        vat_code=None,
    )
    with pytest.raises(XeroError) as e:
        XeroVatCode.objects.vat_code_for(VatCode.ZERO_RATE_EC)
    assert "VAT code 'ZEU' not set-up in Xero VAT settings" in str(e.value)
