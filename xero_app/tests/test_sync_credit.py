# -*- encoding: utf-8 -*-
import os
import pytest
import responses
import uuid

from datetime import date
from decimal import Decimal
from freezegun import freeze_time
from http import HTTPStatus

from contact.tests.factories import ContactFactory
from finance.models import Currency, VatCode
from invoice.models import InvoiceCredit, InvoiceIssue
from invoice.tests.factories import (
    BatchFactory,
    BatchInvoiceFactory,
    InvoiceContactFactory,
    InvoiceCreditFactory,
    InvoiceFactory,
    InvoiceLineFactory,
)
from xero_app.models import ContactUUID, InvoiceUUID, SyncCredit, XeroError
from .factories import (
    AccountFactory,
    InvoiceUUIDFactory,
    XeroCurrencyFactory,
    XeroSettingsFactory,
    XeroVatCodeFactory,
)


@pytest.mark.django_db
def test_credit_note_payment_to_xero_data():
    sync_credit = SyncCredit()
    credit_note = InvoiceFactory(
        invoice_date=date(2017, 5, 21),
        exchange_rate=Decimal("1.000"),
        is_credit=True,
    )
    InvoiceLineFactory(
        invoice=credit_note,
        description="Apple",
        quantity=Decimal("-1"),
        price=Decimal("10"),
    ).save_and_calculate()
    assert {
        "Amount": "-12.00",
        "Account": {"AccountID": "0333"},
        "CreditNote": {"CreditNoteID": "RED-123"},
        "CurrencyRate": "1.0000",
        "Date": date(2017, 5, 21),
        "Reference": "",
    } == sync_credit._credit_note_payment_to_xero_data(
        credit_note,
        "RED-123",
        AccountFactory(code="0333", field_name="AccountID"),
    )


@pytest.mark.django_db
def test_credit_note_payment_to_xero_data_euro(settings):
    settings.XERO_KEY_FILE = os.path.join(
        os.path.dirname(os.path.realpath(__file__)), "data", "privatekey.pem"
    )
    sync_credit = SyncCredit()
    credit_note = InvoiceFactory(
        currency=Currency.objects.get(slug=Currency.EURO),
        exchange_rate=Decimal("0.880"),
        invoice_date=date(2017, 5, 21),
        is_credit=True,
    )
    InvoiceLineFactory(
        description="Apple",
        invoice=credit_note,
        price=Decimal("10"),
        quantity=Decimal("-1"),
    ).save_and_calculate()
    assert {
        "Account": {"AccountID": "0333"},
        "Amount": "-12.00",
        "CreditNote": {"CreditNoteID": "RED-123"},
        "CurrencyRate": "1.1364",
        "Date": date(2017, 5, 21),
        "Reference": "",
    } == sync_credit._credit_note_payment_to_xero_data(
        credit_note,
        "RED-123",
        AccountFactory(code="0333", field_name="AccountID"),
    )


@pytest.mark.django_db
def test_credit_note_to_xero_data():
    contact = ContactFactory()
    credit_note = InvoiceFactory(
        contact=contact,
        invoice_date=date(2017, 5, 21),
        exchange_rate=Decimal("1.000"),
        is_credit=True,
    )
    InvoiceLineFactory(
        invoice=credit_note,
        description="Apple",
        quantity=Decimal("-1"),
        price=Decimal("10"),
    ).save_and_calculate()
    # currency
    currency = Currency.objects.get(slug=Currency.POUND)
    XeroSettingsFactory(base_currency=currency)
    xero_currency = XeroCurrencyFactory(code="GBP", currency=currency)
    # vat
    XeroVatCodeFactory(code="SS", vat_code=VatCode.objects.get(slug="S"))
    # get the contact uuid
    ContactUUID.objects.init_contact(contact)
    x = str(uuid.uuid4())
    ContactUUID.objects.update_uuid(contact, x)
    contact_uuid = ContactUUID.objects.get_if_exists(contact)
    assert contact_uuid is not None
    # test
    sync_credit = SyncCredit()
    with freeze_time("2017-05-21 23:55:01"):
        result = sync_credit._credit_note_to_xero_data(
            credit_note, credit_note.contact, xero_currency, "222"
        )
    assert {
        "Type": "ACCRECCREDIT",
        "CreditNoteNumber": credit_note.invoice_number,
        "Contact": {"ContactID": contact_uuid.uuid_as_str()},
        "CurrencyCode": "GBP",
        "CurrencyRate": "1.0000",
        "Date": date(2017, 5, 21),
        "Payments": [],
        "Reference": "",
        "Status": "AUTHORISED",
        "SubTotal": "-10.00",
        "Total": "-12.00",
        "TotalTax": "-2.00",
        "UpdatedDateUTC": "2017-05-21T23:55:01+00:00",
        "LineAmountTypes": "Exclusive",
        "LineItems": [
            {
                "AccountCode": "222",
                "Description": "Summary for VAT code 'S' (1 items)",
                "LineAmount": "-10.00",
                "Quantity": "1.0",
                "TaxAmount": "-2.00",
                "TaxType": "SS",
                "Tracking": [],
                "UnitAmount": "-10.00",
                "ValidationErrors": [],
            }
        ],
    } == result


@pytest.mark.django_db
@responses.activate
def test_sync_credit_note_on_account(settings):
    """Sync a credit note for an invoice.

    Example A.

    1. The credit note is linked to an invoice
    2. The invoice is not part of a batch
    3. The credit note has not been posted
    4. Post to Xero

    .. note:: Test code copied from
              ``test_sync_date_credit_note_not_allocated_to_a_batch``
              (see below).

    """
    settings.XERO_KEY_FILE = os.path.join(
        os.path.dirname(os.path.realpath(__file__)), "data", "privatekey.pem"
    )
    contact_uuid = str(uuid.uuid4())
    credit_note_uuid = str(uuid.uuid4())
    responses.add(
        responses.PUT,
        "https://api.xero.com/api.xro/2.0/Contacts",
        json={"Status": "OK", "Contacts": [{"ContactID": contact_uuid}]},
        status=HTTPStatus.OK,
    )
    responses.add(
        responses.PUT,
        "https://api.xero.com/api.xro/2.0/CreditNotes",
        json={
            "Status": "OK",
            "CreditNotes": [{"CreditNoteID": credit_note_uuid}],
        },
        status=HTTPStatus.OK,
    )
    base_currency = Currency.objects.get(slug=Currency.POUND)
    XeroSettingsFactory(
        days_to_sync=6,
        invoice_account=AccountFactory(code="010101"),
        base_currency=base_currency,
    )
    XeroCurrencyFactory(code="GBP", currency=base_currency)
    contact = ContactFactory()
    InvoiceContactFactory(contact=contact, on_account=True)
    invoice_date = date(2019, 7, 21)
    invoice = InvoiceFactory(
        contact=contact, invoice_date=invoice_date, is_credit=False
    )
    credit_note = InvoiceFactory(
        contact=contact,
        invoice_date=invoice_date,
        is_credit=True,
        exchange_rate=Decimal("1.000"),
    )
    InvoiceCredit.objects.init_invoice_credit(invoice, credit_note)
    # test
    sync_credit = SyncCredit()
    assert 1 == sync_credit.sync_credit_note_on_account(credit_note)
    invoice_uuid = InvoiceUUID.objects.get(uuid=str(credit_note_uuid))
    assert credit_note == invoice_uuid.invoice


@pytest.mark.django_db
def test_sync_credit_note_on_account_already_posted():
    """Sync a credit note for an invoice.

    Example B:

    1. The credit note has already been posted.

    """
    sync_credit = SyncCredit()
    contact = ContactFactory()
    credit_note = InvoiceFactory(contact=contact, is_credit=True)
    InvoiceCreditFactory(
        invoice=InvoiceFactory(contact=contact), credit_note=credit_note
    )
    InvoiceUUIDFactory(invoice=credit_note)
    assert 0 == sync_credit.sync_credit_note_on_account(credit_note)


@pytest.mark.django_db
def test_sync_credit_note_on_account_in_a_batch():
    """Sync a credit note for an invoice.

    Example C.

    1. The credit note is linked to an invoice
    2. The invoice is part of a batch
    3. ``sync_credit_note_on_account`` will raise an exception because it
       cannot post credit notes for invoices which are part of a batch.

    """
    contact = ContactFactory()
    invoice = InvoiceFactory(contact=contact)
    BatchInvoiceFactory(batch=BatchFactory(), invoice=invoice)
    credit_note = InvoiceFactory(contact=contact, is_credit=True)
    InvoiceCredit.objects.init_invoice_credit(invoice, credit_note)
    # test
    sync_credit = SyncCredit()
    with pytest.raises(XeroError) as e:
        sync_credit.sync_credit_note_on_account(credit_note)
    assert (
        "Credit note '{}' cannot be posted using this method. "
        "It is linked to invoice '{}' which is in a batch".format(
            credit_note.pk, invoice.pk
        )
    ) in str(e.value)


@pytest.mark.django_db
@responses.activate
def test_sync_credit_note_on_account_not_linked_to_invoice():
    """Sync a credit note for an invoice.

    Example A.

    1. The credit note is *not* linked to an invoice
    2. The contact is *not* setup for on-account postings
    3. The credit note has not been posted
    4. Do not post to Xero

    .. note:: Test code copied from
              ``test_sync_date_credit_note_not_allocated_to_a_batch``
              (see below).

    """
    contact_uuid = str(uuid.uuid4())
    credit_note_uuid = str(uuid.uuid4())
    responses.add(
        responses.PUT,
        "https://api.xero.com/api.xro/2.0/Contacts",
        json={"Status": "OK", "Contacts": [{"ContactID": contact_uuid}]},
        status=HTTPStatus.OK,
    )
    responses.add(
        responses.PUT,
        "https://api.xero.com/api.xro/2.0/CreditNotes",
        json={
            "Status": "OK",
            "CreditNotes": [{"CreditNoteID": credit_note_uuid}],
        },
        status=HTTPStatus.OK,
    )
    base_currency = Currency.objects.get(slug=Currency.POUND)
    XeroSettingsFactory(
        days_to_sync=6,
        invoice_account=AccountFactory(code="010101"),
        base_currency=base_currency,
    )
    XeroCurrencyFactory(code="GBP", currency=base_currency)
    contact = ContactFactory()
    invoice_date = date(2019, 7, 21)
    invoice = InvoiceFactory(
        contact=contact, invoice_date=invoice_date, is_credit=False
    )
    credit_note = InvoiceFactory(
        contact=contact,
        invoice_date=invoice_date,
        is_credit=True,
        exchange_rate=Decimal("1.000"),
    )
    # InvoiceCredit.objects.init_invoice_credit(invoice, credit_note)
    # test
    sync_credit = SyncCredit()
    with pytest.raises(XeroError) as e:
        sync_credit.sync_credit_note_on_account(credit_note)
    assert (
        "Credit note '{}' cannot be posted using this method. The contact "
        "is not setup for 'on_account' postings".format(credit_note.pk)
    )
    # assert 1 == InvoiceIssue.objects.count()
    # invoice_issue = InvoiceIssue.objects.first()
    # assert credit_note == invoice_issue.invoice
    # assert ["abc"] == invoice_issue.lines()


@pytest.mark.django_db
@responses.activate
def test_sync_credit_note_on_account_not_linked_to_invoice_but_on_account(
    settings,
):
    """Sync a credit note for an invoice.

    Example A.

    1. The credit note is *not* linked to an invoice
    2. The contact is setup for on-account postings
    3. The invoice is not part of a batch
    4. The credit note has not been posted
    5. Post to Xero

    .. note:: Test code copied from
              ``test_sync_date_credit_note_not_allocated_to_a_batch``
              (see below).

    """
    settings.XERO_KEY_FILE = os.path.join(
        os.path.dirname(os.path.realpath(__file__)), "data", "privatekey.pem"
    )
    contact_uuid = str(uuid.uuid4())
    credit_note_uuid = str(uuid.uuid4())
    responses.add(
        responses.PUT,
        "https://api.xero.com/api.xro/2.0/Contacts",
        json={"Status": "OK", "Contacts": [{"ContactID": contact_uuid}]},
        status=HTTPStatus.OK,
    )
    responses.add(
        responses.PUT,
        "https://api.xero.com/api.xro/2.0/CreditNotes",
        json={
            "Status": "OK",
            "CreditNotes": [{"CreditNoteID": credit_note_uuid}],
        },
        status=HTTPStatus.OK,
    )
    base_currency = Currency.objects.get(slug=Currency.POUND)
    XeroSettingsFactory(
        days_to_sync=6,
        invoice_account=AccountFactory(code="010101"),
        base_currency=base_currency,
    )
    XeroCurrencyFactory(code="GBP", currency=base_currency)
    contact = ContactFactory()
    InvoiceContactFactory(contact=contact, on_account=True)
    invoice_date = date(2019, 7, 21)
    invoice = InvoiceFactory(
        contact=contact, invoice_date=invoice_date, is_credit=False
    )
    credit_note = InvoiceFactory(
        contact=contact,
        invoice_date=invoice_date,
        is_credit=True,
        exchange_rate=Decimal("1.000"),
    )
    # InvoiceCredit.objects.init_invoice_credit(invoice, credit_note)
    # test
    sync_credit = SyncCredit()
    assert 1 == sync_credit.sync_credit_note_on_account(credit_note)
    invoice_uuid = InvoiceUUID.objects.get(uuid=str(credit_note_uuid))
    assert credit_note == invoice_uuid.invoice


@pytest.mark.django_db
@responses.activate
def test_sync_date_credit_note_not_allocated_to_a_batch():
    """

    Test setup copied to:
    ``test_sync_credit_note_to_xero`` in ``test_management_command.py``.

    .. note:: Test code copied to ``test_sync_credit_note_on_account``
              (see above).

    """
    contact_uuid = str(uuid.uuid4())
    credit_note_uuid = str(uuid.uuid4())
    responses.add(
        responses.PUT,
        "https://api.xero.com/api.xro/2.0/Contacts",
        json={"Status": "OK", "Contacts": [{"ContactID": contact_uuid}]},
        status=HTTPStatus.OK,
    )
    responses.add(
        responses.PUT,
        "https://api.xero.com/api.xro/2.0/CreditNotes",
        json={
            "Status": "OK",
            "CreditNotes": [{"CreditNoteID": credit_note_uuid}],
        },
        status=HTTPStatus.OK,
    )
    base_currency = Currency.objects.get(slug=Currency.POUND)
    XeroSettingsFactory(
        days_to_sync=6,
        invoice_account=AccountFactory(code="010101"),
        base_currency=base_currency,
    )
    XeroCurrencyFactory(code="GBP", currency=base_currency)
    contact = ContactFactory()
    invoice_date = date(2019, 7, 21)
    invoice = InvoiceFactory(
        contact=contact, invoice_date=invoice_date, is_credit=False
    )
    credit_note = InvoiceFactory(
        contact=contact,
        invoice_date=invoice_date,
        is_credit=True,
        exchange_rate=Decimal("1.000"),
    )
    InvoiceCredit.objects.init_invoice_credit(invoice, credit_note)
    # test
    sync_credit = SyncCredit()
    sync_credit.sync_date(date(2019, 7, 21))
    assert 1 == InvoiceIssue.objects.count()
    invoice_issue = InvoiceIssue.objects.first()
    assert credit_note == invoice_issue.invoice
    assert [
        "The customer is not setup for on-account postings.",
        "The credit note is not linked to a batched invoice.",
    ] == invoice_issue.lines()


@pytest.mark.django_db
def test_sync_date_credit_note_not_linked():
    credit_note = InvoiceFactory(
        contact=ContactFactory(), invoice_date=date(2019, 7, 21), is_credit=True
    )
    sync_credit = SyncCredit()
    sync_credit.sync_date(date(2019, 7, 21))
    assert 1 == InvoiceIssue.objects.count()
    invoice_issue = InvoiceIssue.objects.first()
    assert credit_note == invoice_issue.invoice
    assert [
        "The customer is not setup for on-account postings.",
        "The credit note is not linked to a batched invoice.",
    ] == invoice_issue.lines()
