# -*- encoding: utf-8 -*-
import os
import pytest
import responses
import uuid

from datetime import date
from decimal import Decimal
from freezegun import freeze_time
from http import HTTPStatus

from contact.tests.factories import ContactFactory
from finance.models import Currency, VatCode
from invoice.tests.factories import InvoiceFactory, InvoiceLineFactory
from xero_app.models import ContactUUID, InvoiceUUID, SyncInvoice, XeroError
from .factories import (
    AccountFactory,
    XeroCurrencyFactory,
    XeroSettingsFactory,
    XeroVatCodeFactory,
)


@pytest.mark.django_db
def test_invoice_to_xero_data():
    contact = ContactFactory()
    invoice = InvoiceFactory(
        contact=contact,
        invoice_date=date(2017, 5, 21),
        exchange_rate=Decimal("1.000"),
    )
    InvoiceLineFactory(
        invoice=invoice,
        description="Apple",
        quantity=Decimal("1"),
        price=Decimal("10"),
    ).save_and_calculate()
    InvoiceLineFactory(
        invoice=invoice,
        description="Orange",
        quantity=Decimal("2"),
        price=Decimal("30"),
    ).save_and_calculate()
    # currency
    currency = Currency.objects.get(slug=Currency.POUND)
    XeroSettingsFactory(base_currency=currency)
    xero_currency = XeroCurrencyFactory(code="GBP", currency=currency)
    # vat
    XeroVatCodeFactory(code="SS", vat_code=VatCode.objects.get(slug="S"))
    # get the contact uuid
    ContactUUID.objects.init_contact(contact)
    x = str(uuid.uuid4())
    ContactUUID.objects.update_uuid(contact, x)
    contact_uuid = ContactUUID.objects.get_if_exists(contact)
    assert contact_uuid is not None
    # test
    sync_invoice = SyncInvoice()
    with freeze_time("2017-05-21 23:55:01"):
        result = sync_invoice._invoice_to_xero_data(
            invoice, xero_currency, "222"
        )
    assert {
        "Type": "ACCREC",
        # "InvoiceID": invoice.pk,
        "InvoiceNumber": invoice.invoice_number,
        "Reference": "",
        "Payments": [],
        "CreditNotes": [],
        "Prepayments": [],
        "Overpayments": [],
        "AmountDue": invoice.gross,
        "AmountPaid": 0.0,
        "AmountCredited": 0.0,
        "CurrencyRate": "1.0000",
        "HasErrors": False,
        "IsDiscounted": False,
        "HasAttachments": False,
        "Contact": {"ContactID": contact_uuid.uuid_as_str()},
        # "DateString": "2017-05-21",
        "Date": date(2017, 5, 21),
        # "DueDateString": "2017-05-21T23:55:01+00:00",
        "DueDate": date(2017, 5, 21),
        "Status": "AUTHORISED",
        "LineAmountTypes": "Exclusive",
        "LineItems": [
            {
                "Description": "Summary for VAT code 'S' (2 items)",
                "UnitAmount": "70.00",
                "TaxType": "SS",
                "TaxAmount": "14.00",
                # "LineAmount": "10.00",
                "AccountCode": "222",
                # "Tracking": [],
                "Quantity": "1.00",
            }
        ],
        "SubTotal": "70.00",
        "TotalTax": "14.00",
        "Total": "84.00",
        "UpdatedDateUTC": "2017-05-21T23:55:01+00:00",
        "CurrencyCode": "GBP",
        "Url": "http://localhost/invoice/{}/".format(invoice.pk),
    } == result


@pytest.mark.django_db
@responses.activate
def test_sync_date(settings):
    settings.XERO_KEY_FILE = os.path.join(
        os.path.dirname(os.path.realpath(__file__)), "data", "privatekey.pem"
    )
    contact_uuid = str(uuid.uuid4())
    invoice_uuid = str(uuid.uuid4())
    responses.add(
        responses.POST,
        "https://api.xero.com/api.xro/2.0/Contacts",
        json={"Status": "OK", "Contacts": [{"ContactID": contact_uuid}]},
        status=HTTPStatus.OK,
    )
    responses.add(
        responses.PUT,
        "https://api.xero.com/api.xro/2.0/Contacts",
        json={"Status": "OK", "Contacts": [{"ContactID": contact_uuid}]},
        status=HTTPStatus.OK,
    )
    responses.add(
        responses.PUT,
        "https://api.xero.com/api.xro/2.0/Invoices",
        json={"Status": "OK", "Invoices": [{"InvoiceID": invoice_uuid}]},
        status=HTTPStatus.OK,
    )
    base_currency = Currency.objects.get(slug=Currency.POUND)
    XeroSettingsFactory(
        days_to_sync=6,
        invoice_account=AccountFactory(code="010101"),
        base_currency=base_currency,
    )
    XeroCurrencyFactory(code="GBP", currency=base_currency)
    contact = ContactFactory()
    invoice_date = date(2019, 7, 21)
    invoice = InvoiceFactory(
        contact=contact,
        invoice_date=invoice_date,
        is_credit=False,
        exchange_rate=Decimal("1.000"),
    )
    # test
    sync_invoice = SyncInvoice()
    sync_invoice.sync_date(invoice_date)
    assert 1 == sync_invoice.sync_date(invoice_date)
    invoice_uuid = InvoiceUUID.objects.get(uuid=str(invoice_uuid))
    assert invoice == invoice_uuid.invoice


@pytest.mark.django_db
def test_xero_account_for_invoice_from_settings():
    account = AccountFactory()
    XeroSettingsFactory(invoice_account=account)
    sync_invoice = SyncInvoice()
    assert account == sync_invoice._xero_account_for_invoice()


@pytest.mark.django_db
def test_xero_account_for_invoice_no_settings():
    sync_invoice = SyncInvoice()
    with pytest.raises(XeroError) as e:
        sync_invoice._xero_account_for_invoice()
    assert "Cannot find a Xero account for invoices" in str(e.value)
