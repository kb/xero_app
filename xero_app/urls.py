# -*- encoding: utf-8 -*-
from django.conf.urls import url
from django.urls import path

from .views import (
    BatchAccountListView,
    BatchAccountUpdateView,
    XeroBatchDetailView,
    XeroBatchSyncUpdateView,
    XeroBatchSyncPaymentUpdateView,
    XeroCurrencyListView,
    XeroCurrencyUpdateView,
    XeroInvoiceDetailView,
    XeroInvoiceSyncPaymentUpdateView,
    XeroInvoiceSyncUpdateView,
    XeroSettingsUpdateView,
    XeroVatCodeListView,
    XeroVatCodeUpdateView,
)


urlpatterns = [
    url(
        regex=r"^batch/account/$",
        view=BatchAccountListView.as_view(),
        name="xero.account.list",
    ),
    url(
        regex=r"^batch/account/(?P<pk>\d+)/update/$",
        view=BatchAccountUpdateView.as_view(),
        name="xero.account.update",
    ),
    url(
        regex=r"^currency/$",
        view=XeroCurrencyListView.as_view(),
        name="xero.currency.list",
    ),
    url(
        regex=r"^currency/(?P<pk>\d+)/update/$",
        view=XeroCurrencyUpdateView.as_view(),
        name="xero.currency.update",
    ),
    path(
        "batch/<int:pk>/",
        view=XeroBatchDetailView.as_view(),
        name="xero.batch.detail",
    ),
    path(
        "batch/<int:pk>/sync/",
        view=XeroBatchSyncUpdateView.as_view(),
        name="xero.batch.sync",
    ),
    path(
        "batch/<int:pk>/sync/payment/",
        view=XeroBatchSyncPaymentUpdateView.as_view(),
        name="xero.batch.sync.payment",
    ),
    path(
        "invoice/<int:pk>/",
        view=XeroInvoiceDetailView.as_view(),
        name="xero.invoice.detail",
    ),
    path(
        "invoice/<int:pk>/sync/",
        view=XeroInvoiceSyncUpdateView.as_view(),
        name="xero.invoice.sync",
    ),
    path(
        "invoice/<int:pk>/sync/payment/",
        view=XeroInvoiceSyncPaymentUpdateView.as_view(),
        name="xero.invoice.sync.payment",
    ),
    url(
        regex=r"^settings/update/$",
        view=XeroSettingsUpdateView.as_view(),
        name="xero.settings.update",
    ),
    url(
        regex=r"^vat/code/$",
        view=XeroVatCodeListView.as_view(),
        name="xero.vat.code.list",
    ),
    url(
        regex=r"^vat/code/(?P<pk>\d+)/update/$",
        view=XeroVatCodeUpdateView.as_view(),
        name="xero.vat.code.update",
    ),
]
