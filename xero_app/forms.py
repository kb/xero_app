# -*- encoding: utf-8 -*-
from django import forms

from .models import BatchAccount, XeroCurrency, XeroSettings, XeroVatCode


class BatchAccountUpdateForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name in ("invoice_account", "payment_account"):
            f = self.fields[name]
            f.widget.attrs.update({"class": "chosen-select"})

    class Meta:
        model = BatchAccount
        fields = ("invoice_account", "payment_account")

    def clean(self):
        cleaned_data = super().clean()
        invoice_account = cleaned_data.get("invoice_account")
        payment_account = cleaned_data.get("payment_account")
        if invoice_account and payment_account:
            pass
        else:
            raise forms.ValidationError(
                "Please select an invoice and payment account.",
                code="batch_account__invoice_and_payment_account",
            )
        return cleaned_data


class XeroCurrencyUpdateForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name in ("currency",):
            f = self.fields[name]
            f.widget.attrs.update({"class": "chosen-select"})

    class Meta:
        model = XeroCurrency
        fields = ("currency",)

    def clean(self):
        cleaned_data = super().clean()
        currency = cleaned_data.get("currency")
        if currency:
            pass
        else:
            raise forms.ValidationError(
                "Please select a currency and VAT codes.",
                code="xero_currency__currency_and_vat_code",
            )
        return cleaned_data


class XeroSettingsForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        f = self.fields["base_currency"]
        for name in ("base_currency", "invoice_account"):
            f = self.fields[name]
            f.required = False
            f.widget.attrs.update({"class": "chosen-select"})

    class Meta:
        model = XeroSettings
        fields = ("days_to_sync", "invoice_account", "base_currency")

    def clean(self):
        """Validation for the Xero settings.

        The ``base_currency`` and ``vat_code_zero_rated_ec`` are not required
        fields in the model because it was added after the system was
        initialised.

        """
        cleaned_data = super().clean()
        invoice_account = cleaned_data.get("invoice_account")
        if invoice_account:
            pass
        else:
            raise forms.ValidationError(
                "Please select the default account "
                "for posting individual invoices",
                code="xero_settings__invoice_account",
            )
        base_currency = cleaned_data.get("base_currency")
        if base_currency:
            pass
        else:
            raise forms.ValidationError(
                "You must select a base currency",
                code="xero_settings__base_currency",
            )
        return cleaned_data

    def clean_days_to_sync(self):
        data = self.cleaned_data.get("days_to_sync")
        days_to_sync = int(data)
        if not days_to_sync:
            raise forms.ValidationError(
                "Days to sync must be greater than 1",
                code="xero_settings__days_to_sync_gt_0",
            )
        return data


class XeroVatCodeUpdateForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name in ("vat_code",):
            f = self.fields[name]
            f.widget.attrs.update({"class": "chosen-select"})

    class Meta:
        model = XeroVatCode
        fields = ("vat_code",)

    def clean(self):
        cleaned_data = super().clean()
        vat_code = cleaned_data.get("vat_code")
        if vat_code:
            try:
                check = XeroVatCode.objects.get(vat_code=vat_code)
                if check == self.instance:
                    pass
                else:
                    raise forms.ValidationError(
                        "VAT Code '{}' is already linked to Xero "
                        "VAT code '{}'".format(vat_code.slug, check.code),
                        code="xero_vat_code__vat_code__exists",
                    )
            except XeroVatCode.DoesNotExist:
                pass
        return cleaned_data
