Xero App
********

Xero App

.. note:: The app is called ``xero_app`` because the ``pyxero`` app uses
          ``from xero import`` and I didn't want the app to clash.

Install
=======

Virtual Environment
-------------------

::

  virtualenv --python=python3 venv-xero-app
  # or
  python3 -m venv venv-xero-app
  source venv-xero-app/bin/activate

  pip install -r requirements/local.txt

Testing
=======

::

  find . -name '*.pyc' -delete
  pytest -x

Usage
=====

::

  ./init_dev.sh

Release
=======

https://www.kbsoftware.co.uk/docs/
