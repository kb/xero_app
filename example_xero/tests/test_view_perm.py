# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse

from contact.tests.factories import UserContactFactory
from invoice.tests.factories import BatchFactory, InvoiceFactory
from login.tests.fixture import perm_check


@pytest.mark.django_db
def test_batch_invoice_list(perm_check):
    batch = BatchFactory()
    url = reverse("invoice.batch.invoice.list", args=[batch.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_contact_detail(perm_check):
    user_contact = UserContactFactory()
    url = reverse("contact.detail", args=[user_contact.contact.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_invoice_detail(perm_check):
    invoice = InvoiceFactory()
    url = reverse("invoice.detail", args=[invoice.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_invoice_list(perm_check):
    url = reverse("invoice.list")
    perm_check.staff(url)
