# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from http import HTTPStatus
from unittest import mock

from contact.tests.factories import UserContactFactory
from invoice.tests.factories import BatchFactory, InvoiceFactory
from login.tests.factories import TEST_PASSWORD, UserFactory
from xero_app.tests.factories import (
    BatchPaymentUUIDFactory,
    BatchUUIDFactory,
    InvoiceUUIDFactory,
    PaymentUUIDFactory,
)


@pytest.mark.django_db
def test_batch_invoice_list(client):
    batch = BatchFactory()
    BatchPaymentUUIDFactory(
        batch=batch, uuid="059b59ec-baf0-4e2b-99a7-c6ad83addd92"
    )
    BatchUUIDFactory(batch=batch, uuid="96df0dff-43ec-4899-a7d9-e9d63ef12b19")
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    with mock.patch(
        "xero_app.models.get_xero_invoice_data"
    ) as xero_invoice_data:
        xero_invoice_data.return_value = {
            "OnlineInvoices": [
                {"OnlineInvoiceUrl": "http://test.xero.invoice.url/"}
            ]
        }
        response = client.get(
            reverse("invoice.batch.invoice.list", args=[batch.pk])
        )
    assert HTTPStatus.OK == response.status_code
    assert "xero_batch_url" in response.context
    assert "xero_batch_uuid" in response.context
    assert "xero_payment_uuid" in response.context
    assert "http://test.xero.invoice.url/" == response.context["xero_batch_url"]
    assert (
        "96df0dff-43ec-4899-a7d9-e9d63ef12b19"
        == response.context["xero_batch_uuid"]
    )
    assert (
        "059b59ec-baf0-4e2b-99a7-c6ad83addd92"
        == response.context["xero_payment_uuid"]
    )


@pytest.mark.django_db
def test_invoice_detail(client):
    invoice = InvoiceFactory()
    PaymentUUIDFactory(
        invoice=invoice, uuid="059b59ec-baf0-4e2b-99a7-c6ad83addd92"
    )
    InvoiceUUIDFactory(
        invoice=invoice, uuid="96df0dff-43ec-4899-a7d9-e9d63ef12b19"
    )
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    with mock.patch(
        "xero_app.models.get_xero_invoice_data"
    ) as xero_invoice_data:
        xero_invoice_data.return_value = {
            "OnlineInvoices": [
                {"OnlineInvoiceUrl": "http://test.xero.invoice.url/"}
            ]
        }
        response = client.get(reverse("invoice.detail", args=[invoice.pk]))
    assert HTTPStatus.OK == response.status_code
    assert "xero_invoice_url" in response.context
    assert "xero_invoice_uuid" in response.context
    assert "xero_payment_uuid" in response.context
    assert (
        "http://test.xero.invoice.url/" == response.context["xero_invoice_url"]
    )
    assert (
        "96df0dff-43ec-4899-a7d9-e9d63ef12b19"
        == response.context["xero_invoice_uuid"]
    )
    assert (
        "059b59ec-baf0-4e2b-99a7-c6ad83addd92"
        == response.context["xero_payment_uuid"]
    )
