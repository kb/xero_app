# -*- encoding: utf-8 -*-
import pytest

from django.core.management import call_command

from finance.models import Currency
from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_create_test_invoice():
    UserFactory(username="staff")
    call_command("create_test_invoice", Currency.POUND)


@pytest.mark.django_db
def test_demo_data():
    call_command("demo_data_xero")
