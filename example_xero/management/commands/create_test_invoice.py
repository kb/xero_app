# -*- encoding: utf-8 -*-
import logging

from datetime import date
from decimal import Decimal
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from django.db import transaction

from contact.models import Contact
from invoice.models import Invoice, InvoiceContact, InvoiceLine, InvoiceSettings
from finance.models import Currency, VatCode, VatSettings
from stock.models import Product, ProductCategory, ProductType
from xero_app.models import XeroError


logger = logging.getLogger(__name__)


class Command(BaseCommand):

    help = "Create a test invoice..."

    def _contact(self, user):
        try:
            contact = Contact.objects.get(user=user)
        except Contact.DoesNotExist:
            contact = Contact.objects.create_contact(user)
        try:
            invoice_contact = InvoiceContact.objects.get(contact=contact)
            if invoice_contact.hourly_rate:
                pass
            else:
                invoice_contact.hourly_rate = Decimal("31")
                invoice_contact.save()
        except InvoiceContact.DoesNotExist:
            InvoiceContact(contact=contact, hourly_rate=Decimal("30")).save()
        return contact

    def _currency(self, slug):
        try:
            currency = Currency.objects.get(slug=slug)
        except Currency.DoesNotExist:
            message = [x.slug for x in Currency.objects.all()]
            raise XeroError(
                "Currency '{}' does not exist. Try one of {}".format(
                    slug, message
                )
            )
        return currency

    def _product(self):
        product_type = ProductType.objects.init_product_type("fresh", "Fresh")
        product_category = ProductCategory.objects.init_product_category(
            "fruit", "Fruit", product_type
        )
        product = Product.objects.init_product(
            "apple", "Apple", "Apple", Decimal("0.50"), product_category
        )
        return product

    def _settings(self, product):
        try:
            invoice_settings = InvoiceSettings.objects.get()
            if product == invoice_settings.time_record_product:
                pass
            else:
                invoice_settings.time_record_product = product
                invoice_settings.save()
        except InvoiceSettings.DoesNotExist:
            InvoiceSettings(time_record_product=product).save()
        try:
            VatSettings.objects.get()
        except VatSettings.DoesNotExist:
            VatSettings().save()

    def add_arguments(self, parser):
        parser.add_argument("currency", nargs="?", type=str)

    def handle(self, *args, **options):
        self.stdout.write(self.help)
        currency_slug = options["currency"]
        product = self._product()
        self._settings(product)
        currency = self._currency(currency_slug)
        vat_code = VatCode.objects.get(slug=VatCode.STANDARD)
        user = get_user_model().objects.get(username="staff")
        with transaction.atomic():
            contact = self._contact(user)
            invoice = Invoice(
                contact=contact,
                currency=currency,
                invoice_date=date.today(),
                number=Invoice.objects.next_number(),
                user=user,
            )
            invoice.save()
            invoice_line = InvoiceLine(
                invoice=invoice,
                line_number=invoice.get_next_line_number(),
                product=product,
                description="Pink Lady",
                quantity=Decimal("100"),
                units="each",
                price=Decimal("1"),
                vat_code=vat_code,
                user=user,
            )
            invoice_line.save_and_calculate()
        self.stdout.write(
            "Created invoice number {}".format(invoice.invoice_number)
        )
        self.stdout.write(
            "To post this invoice to Xero:\n"
            "django-admin.py sync_invoice_to_xero {}".format(invoice.pk)
        )
        self.stdout.write("Complete - {}".format(self.help))
