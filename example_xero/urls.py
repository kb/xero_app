# -*- encoding: utf-8 -*-
from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path

from .views import (
    BatchInvoiceListView,
    ContactDetailView,
    DashView,
    HomeView,
    InvoiceDetailView,
    InvoiceListView,
    SettingsView,
)


admin.autodiscover()


urlpatterns = [
    path("admin/", admin.site.urls),
    url(regex=r"^", view=include("login.urls")),
    url(regex=r"^$", view=HomeView.as_view(), name="project.home"),
    url(
        regex=r"^batch/(?P<pk>\d+)/invoice/$",
        view=BatchInvoiceListView.as_view(),
        name="invoice.batch.invoice.list",
    ),
    url(
        regex=r"^contact/(?P<pk>\d+)/$",
        view=ContactDetailView.as_view(),
        name="contact.detail",
    ),
    url(regex=r"^contact/", view=include("contact.urls")),
    url(regex=r"^dash/$", view=DashView.as_view(), name="project.dash"),
    url(regex=r"^gdpr/", view=include("gdpr.urls")),
    url(
        regex=r"^invoice/(?P<pk>\d+)/$",
        view=InvoiceDetailView.as_view(),
        name="invoice.detail",
    ),
    url(
        regex=r"^invoice/$", view=InvoiceListView.as_view(), name="invoice.list"
    ),
    url(regex=r"^invoice/", view=include("invoice.urls")),
    url(regex=r"^mail/", view=include("mail.urls")),
    url(regex=r"^xero_app/", view=include("xero_app.urls")),
    url(
        regex=r"^settings/$",
        view=SettingsView.as_view(),
        name="project.settings",
    ),
]

urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        url(r"^__debug__/", include(debug_toolbar.urls))
    ] + urlpatterns
