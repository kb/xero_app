# -*- encoding: utf-8 -*-
from braces.views import LoginRequiredMixin, StaffuserRequiredMixin
from django.views.generic import DetailView, ListView, TemplateView

from base.view_utils import BaseMixin
from contact.views import ContactDetailMixin
from invoice.views import (
    BatchInvoiceListMixin,
    InvoiceDetailMixin,
    InvoiceListMixin,
)
from xero_app.views import XeroBatchDetailMixin, XeroInvoiceDetailMixin


class BatchInvoiceListView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    BatchInvoiceListMixin,
    XeroBatchDetailMixin,
    BaseMixin,
    ListView,
):
    template_name = "example/batch_invoice_list.html"


class ContactDetailView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    ContactDetailMixin,
    BaseMixin,
    DetailView,
):
    pass


class DashView(BaseMixin, TemplateView):
    template_name = "example/dash.html"


class HomeView(BaseMixin, TemplateView):
    template_name = "example/home.html"


class InvoiceDetailView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    InvoiceDetailMixin,
    XeroInvoiceDetailMixin,
    BaseMixin,
    DetailView,
):
    template_name = "example/invoice_detail.html"


class InvoiceListView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    InvoiceListMixin,
    BaseMixin,
    ListView,
):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({"show_download": False})
        return context


class SettingsView(BaseMixin, TemplateView):
    template_name = "example/settings.html"
